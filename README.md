# the-john-game
![the-john-game](readme.gif)

## Downloads
- https://gitlab.com/jredmond/ld40/builds/artifacts/master/browse/releases?job=build

## Building and Running
- Install Love2d: https://love2d.org/
- Install ZeroBrane Studio: https://studio.zerobrane.com/
- Clone the repo
- Open main.lua in ZeroBrane
- From the ZeroBrane menu
  - Select Project -> Project Directory -> Set From Current File
  - Set Project -> Lua Interpreter -> LOVE
- F5/F6 to debug/run
local spriteSheets = require("lib.spriteSheets")

local railDrawingHelpers = {}

function railDrawingHelpers.draw(x, y, railConnections)
  railDrawingHelpers.drawEdge(x, y, railConnections[1], 'u')
  railDrawingHelpers.drawEdge(x, y, railConnections[3], 'l')
  railDrawingHelpers.drawEdge(x, y, railConnections[4], 'r')
  railDrawingHelpers.drawEdge(x, y, railConnections[2], 'd')
end

function railDrawingHelpers.drawEdge(x, y, rc, spriteKey)
  if rc ~= 0 then
    local spriteIndex = railDrawingHelpers.spriteIndexLookup[spriteKey]
    local isGhost = rc < 0
    local teamId = math.abs(rc)
    local spriteSheet = spriteSheets.get(teamId)
    local spriteSize = 16
    local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
    
    if isGhost then love.graphics.setColor(255, 255, 255, 60) end
    love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
    if isGhost then love.graphics.setColor(255, 255, 255, 255) end
  end
end

railDrawingHelpers.spriteIndexLookup = {
  ['u'] = { x = 0, y = 6 },
  ['d'] = { x = 1, y = 6 },
  ['l'] = { x = 2, y = 6 },
  ['r'] = { x = 3, y = 6 },
}

return railDrawingHelpers
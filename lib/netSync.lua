local netSync = {}

-- table to hold the changes made this frame
netSync.changes = {}

-- get a new netId
local currId = 0
function netSync.nextId()
  currId = currId + 1
  return currId
end

-- add property changed notification
function netSync.add(netId, field, newValue)
  if not netSync.changes[netId] then 
    netSync.changes[netId] = {} 
  end
  netSync.changes[netId][field] = newValue
end

-- helper function to create a set
function netSync.createSet(...)
  local set = {}
  for i = 1, select("#", ...) do
    local k = select(i, ...)
    set[k] = true
  end
  return set
end

-- key to lookup original entity from the proxy table
netSync.privateKey = {}

local listeningMetatable = {
  -- on get access the original table
  __index = function (t,k) return t[netSync.privateKey][k] end,
  
  -- on set add property changed if new val
  __newindex = function (proxy,k,new)
    local t = proxy[netSync.privateKey]
    if t[k] ~= new then
      t[k] = new   -- update original table
      if t.synced and t.synced[k] then
        print("*update of element " .. tostring(k) .. " to " .. tostring(new))
        netSync.add(t.netId, k, new)
      end
    end
  end
}

-- Create a proxy table to listen for property changes and add them to be transmitted to clients
function netSync.tracked(e)
  local proxy = {}
  proxy[netSync.privateKey] = e -- proxy table holds a private reference to the actual entity table
  setmetatable(proxy, listeningMetatable)
  return proxy
end

return netSync
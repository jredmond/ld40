local love_math = require 'love.math'

local mapGeneration = {}

function mapGeneration.generateTileMap(w, h)
  local map = {}
  for y=0,h-1 do
    map[y] = {}
    for x=0,w-1 do
      -- Currently one of the two asteroid tiles
      map[y][x] = math.random(1, 2)
    end
  end
  return map
end

-- Holds the rail network entities and stockpiles etc. (empty)
function mapGeneration.generateLogisticsMap(w, h)
  local map = {{}}
  for y=0,h-1 do
    map[y] = {}
  end
  return map
end

function mapGeneration.generateOreMap(w, h)
  local xNoiseOffset = w*math.random()
  local yNoiseOffset = h*math.random()
  local map = {}
  for y=0,h-1 do
    map[y] = {}
    for x=0,w-1 do
      local noise = love_math.noise((x+xNoiseOffset)/20, (y+yNoiseOffset)/20)
      if noise > 0.9 then
        map[y][x] = 1
      end
    end
  end
  return map
end

return mapGeneration
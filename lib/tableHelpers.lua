local tableHelpers = {}

-- table to string
function tableHelpers.dump(o)
  if type(o) == 'table' then
    local s = '{ '
    for k,v in pairs(o) do
      if type(k) ~= 'number' then k = '"'..k..'"' end
      s = s .. '['..k..'] = ' .. tableHelpers.dump(v) .. ','
    end
    return s .. '} '
  else
    return tostring(o)
  end
end

function tableHelpers.countElements(e)
  local count = 0
  for key,value in pairs(e) do
    count = count + 1
  end
  return count
end

function tableHelpers.first(list, pred)
  for _,v in ipairs(list) do
    if pred(v) then
      return v
    end
  end
  return nil
end

return tableHelpers
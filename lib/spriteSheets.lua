-- Testing palette swapping
local palettes = { "assets/AsteroidTiles_palette_red.png", "assets/AsteroidTiles_palette_green.png", "assets/AsteroidTiles_palette_blue.png" }
local origSpriteSheet = "assets/AsteroidTiles.png"
local origPalette = love.image.newImageData(palettes[1])
local swatchSize = 14 -- each colour in the palette is 14x14 (as exported from pyxel)
local w, h = origPalette:getWidth()/swatchSize, origPalette:getHeight()/swatchSize

local spriteSheets = {}

-- Find the index of the colour in the palette
local function toPaletteIndex(r,g,b,a)
  for i=0,w*h-1 do
    local x, y = i % w, math.floor(i/w)
    local pr, pg, pb, pa = origPalette:getPixel(x * swatchSize, y * swatchSize)
    if pr == r and pg == g and pb == b and pa == a then
      return i
    end
  end
  error("Pixel colour not in palette at " .. x .. ", " .. y)
end

-- Lookup rgba by index into newPalette
local function toRgb(i, newPalette)
  if i >= w * h then
    error("index not in palette " .. i)
  end
  local x, y = i % w, math.floor(i/w)  
  return newPalette:getPixel(x * swatchSize, y * swatchSize)
end

local function map(func, array)
  local new_array = {}
  for i,v in ipairs(array) do
    new_array[i] = func(v)
  end
  return new_array
end

local function getSpriteSheet(newPalettePath)
  local spriteSheet = love.image.newImageData(origSpriteSheet)
  local newPalette = love.image.newImageData(newPalettePath)
  spriteSheet:mapPixel(function(x,y,r,g,b,a) return toRgb(toPaletteIndex(r, g, b, a), newPalette) end)
  return love.graphics.newImage(spriteSheet)
end

spriteSheets.all = map(getSpriteSheet, palettes)

function spriteSheets.get(teamId)
  local index = (teamId - 1) % #spriteSheets.all + 1
  return spriteSheets.all[index]
end

return spriteSheets
local pathSerialize = {}

local function direction(from, to)
  if from.x > to.x then
    return 'l'
  elseif from.x < to.x then
    return 'r'
  elseif from.y > to.y then
    return 'u'
  elseif from.y < to.y then
    return 'd'
  end
end

function pathSerialize.toString(path)
  local p = ""
  
  local from = path[1]
  
  if from then
    p = p .. from.x .. '|' .. from.y .. '|'
  end
  
  for i=2, #path do
    local to = path[i]
    p = p .. direction(from, to)
    from = to
  end
  
  return p
end

local function getNext(x, y, move)
  if move == 'l' then
    return x - 1, y
  elseif move == 'r' then
    return x + 1, y
  elseif move == 'u' then
    return x, y - 1
  elseif move == 'd' then
    return x, y + 1
  end
end

function pathSerialize.fromString(s)
  local p = {}
  if #s == 0 then return p end
  
  local iter = string.gmatch(s, "[^|]+")
  local x, y = tonumber(iter()), tonumber(iter())
  table.insert(p, { x = x, y = y })
  
  local s = iter()
  if s then
    for i = 1, #s do
      local move = s:sub(i,i)
      x, y = getNext(x, y, move)
      table.insert(p, { x = x, y = y })
    end
  end
  return p
end


return pathSerialize
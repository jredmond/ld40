local multisource = require "lib.multisource"

local assets = {}

-- For non blurry pixels after scaling
love.graphics.setDefaultFilter('nearest', 'nearest')

-- multisource allows the sound effects to overlap without just skipping the overlapping plays
assets.snd_cursorBlip = multisource.new(love.audio.newSource("assets/sndCursorBlip2.wav", "static"))
assets.snd_driveBlip = love.audio.newSource("assets/sndDrive.wav", "static")
assets.snd_transformBlip = multisource.new(love.audio.newSource("assets/TitaniumChimera.wav", "static"))

assets.snd_workerMove = love.audio.newSource("assets/sndWorkerLoop.wav", "static")

assets.snd_music = love.audio.newSource("assets/ambient.wav")

assets.fnt_small = love.graphics.newFont("assets/04B_03__.TTF", 8)

assets.fnt_hud = love.graphics.newFont("assets/04B_03__.TTF", 16)

assets.cursorColours = {
  { 180,020,030 }, -- 01 Red
  { 022,128,000 }, -- 02 Green
  { 000,066,255 }, -- 03 Blue
  { 255,255,255 }, -- 04 White
  { 028,167,234 }, -- 05 Teal
  { 051,026,200 }, -- 06 Purple
  { 235,225,041 }, -- 07 Yellow
  { 254,138,014 }, -- 08 Orange
  { 208,166,252 }, -- 09 Light Pink
  { 031,001,201 }, -- 10 Violet
  { 082,084,148 }, -- 11 Light Grey
  { 016,098,070 }, -- 12 Dark Green
  { 078,072,004 }, -- 13 Brown
  { 150,255,145 }, -- 14 Light Green
  { 035,035,035 }, -- 15 Dark Grey
  { 229,091,176 }  -- 16 Pink
}

return assets
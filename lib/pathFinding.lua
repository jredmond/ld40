local pathFinding = {}

pathFinding.startMarker = 'start'

-- breadth first search
-- @param from Start of search x and y
-- @param atDest Predicate that takes current pos as returns true if we have reach the end of search
-- @param getNeighbours Function that takes current pos and returns a table of neighbours
function pathFinding.findNearest(from, atDest, getNeighbours, w)
  local frontier = { { x = from.x, y = from.y } }
  local cameFrom = {}
  cameFrom[from.y * w + from.x] = pathFinding.startMarker
  local nodesChecked = 0
  
  while #frontier > 0 do
    local current = table.remove(frontier, 1)
    
    if atDest(current) then
      print("from " .. from.x .. " " .. from.y .. " nodes evaluated: " .. nodesChecked)
        -- Reconstruct
      local path = {}
      while current ~= pathFinding.startMarker do
        table.insert(path, 1, current)
        current = cameFrom[current.y * w + current.x]
      end
      return path
    end
    
    local neighbors = getNeighbours(current)
    for _,neigh in ipairs(neighbors) do
      if cameFrom[neigh.y * w + neigh.x] == nil then
        table.insert(frontier, neigh)
        cameFrom[neigh.y * w + neigh.x] = current
        nodesChecked = nodesChecked + 1
      end
    end
  end
  print("nodes exhausted: " .. nodesChecked)
end

-- breadth first search
function pathFinding.getPath(from, to, getNeighbours, w)
  return 
    pathFinding.findNearest(
      from, 
      function(curr) return curr.x == to.x and curr.y == to.y end, 
      getNeighbours,
      w)
end

return pathFinding
local Rover = require "entities.Rover"
local Cursor = require "entities.Cursor"
local Hq = require "entities.Hq"
local Worker = require "entities.Worker"
local Rail = require "entities.Rail"
local Solar = require "entities.Solar"
local LaunchPad = require "entities.LaunchPad"
local Rocket = require "entities.Rocket"
local Stockpile = require "entities.Stockpile"
local Auction = require "entities.Auction"

local netSync = require "lib.netSync"

local netMessaging = {}

netMessaging.serverTiming = nil

function netMessaging.entityToMessage(e)
  local e = class.isInstance(e) and e or e[netSync.privateKey]
  if not class.isInstance(e) then
    print("not a class")
    return { 0, e }
  end
  if e:instanceOf(Cursor) then
    return { 1, e.x, e.y, e.name, e.wealth, e.teamId, e.netId }
  elseif e:instanceOf(Rover) then
    return { 2, e.x, e.y, e.teamId, e.netId }
  elseif e:instanceOf(Hq) then
    return { 3, e.x, e.y, e.teamId, e.resources, e.netId, netMessaging.serverTiming.tick ~= e.buildTick }
  elseif e:instanceOf(Worker) then
    return { 4, e.x, e.y, e.home.x, e.home.y, e.name, e.skills, e.teamId, e.netId }
  elseif e:instanceOf(Rail) then
    return { 5, e.x, e.y, e.railConnections, e.netId }
  elseif e:instanceOf(Solar) then
    return { 6, e.x, e.y, e.teamId, e.remaining, e.netId }
  elseif e:instanceOf(LaunchPad) then
    return { 7, e.x, e.y, e.teamId, e.remaining, e.netId }
  elseif e:instanceOf(Rocket) then
    return { 8, e.x, e.y, e.teamId, e.netId }
  elseif e:instanceOf(Stockpile) then
    return { 9, e.x, e.y, e.resources, e.netId }
  elseif e:instanceOf(Auction) then
    return { 10, e.netId }
  end
end

function netMessaging.messageToEntity(m)
  local typeId = m[1]
  
  if typeId == 0 then
    return m[2]
  elseif typeId == 1 then
    return Cursor(unpack(m, 2))
  elseif typeId == 2 then
    return Rover(unpack(m, 2))
  elseif typeId == 3 then
    return Hq(unpack(m, 2))
  elseif typeId == 4 then
    return Worker(unpack(m, 2))
  elseif typeId == 5 then
    return Rail(unpack(m, 2))
  elseif typeId == 6 then
    return Solar(unpack(m, 2))
  elseif typeId == 7 then
    return LaunchPad(unpack(m, 2))
  elseif typeId == 8 then
    return Rocket(unpack(m, 2))
  elseif typeId == 9 then
    return Stockpile(unpack(m, 2))
  elseif typeId == 10 then
    return Auction(unpack(m, 2))
  end
  return nil
end

local function isEmpty(t)
  return next(t) == nil
end

local function first(list, pred)
  for _,v in ipairs(list) do
    if pred(v) then
      return v
    end
  end
  return nil
end

function netMessaging.buildMessage(changes, entitiesAdded, entitiesRemoved)
  local message = {}
  
  -- property changes
  if not isEmpty(changes) then
    message['c'] = changes
  end
  
  -- additions
  local addMessages = {}
  for _,add in ipairs(entitiesAdded) do
    local m = netMessaging.entityToMessage(add)
    if m ~= nil then
      table.insert(addMessages, m)
    end
  end
  if not isEmpty(addMessages) then
    message['a'] = addMessages
  end
  
  -- removals (by netId)
  local removeMessages = {}
  for _,remove in ipairs(entitiesRemoved) do
    local m = remove.netId
    if m ~= nil then
      table.insert(removeMessages, m)
    end
  end
  if not isEmpty(removeMessages) then
    message['r'] = removeMessages
  end
  
  return not isEmpty(message) and message or nil
end

function netMessaging.applyMessage(world, message)
  
  -- apply the changes
  local changeMessages = message.c
  if changeMessages ~= nil then
    for id,ent in pairs(changeMessages) do
      -- todo replace with direct table index by netId
      local clientEnt = first(world.entities, function(e) return e.netId == id end)
      if clientEnt then
        for prop,val in pairs(ent) do
          clientEnt[prop] = val
        end
      end
    end
  end
  
  -- add new entities (ensuring their netId is the same)
  local addMessages = message.a
  if addMessages ~= nil then
    for _,addMes in ipairs(addMessages) do
      world:add(netMessaging.messageToEntity(addMes))
    end
  end
  
  -- Remove entities by netId
  local removeMessages = message.r
  if removeMessages ~= nil then
    for _,id in ipairs(removeMessages) do
      local e = first(world.entities, function(e) return e.netId == id end)
      world:remove(e)
    end
  end
end

return netMessaging
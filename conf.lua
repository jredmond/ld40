function love.conf(t)
  t.releases = {
    title = 'the-john-game',
    package = 'the-john-game',
    loveVersion = '0.10.2',
    version = nil,
    author = 'John Redmond',
    email = 'redmondjohnf@gmail.com',
    description = nil,
    homepage = nil,
    identifier = 'the-john-game',
    excludeFileList = {},
    releaseDirectory = nil,
  }
end
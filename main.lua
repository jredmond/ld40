tiny = require 'lib.tiny'
class = require 'lib.30log'

local gamestate = require 'lib.gamestate'
local menuState = require 'states.menu'

function love.load()
  -- To allow breakpointing with the love2d interpreter
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  io.stdout:setvbuf("no") -- So print("..") shows up straight away
  
  love.window.setTitle( 'the-john-game' )
  
  gamestate.registerEvents()
  gamestate.switch(menuState)

end
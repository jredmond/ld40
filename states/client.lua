local gamestate = require 'lib.gamestate'
local assets = require 'lib.assets'
local gamera = require 'lib.gamera'
local th = require 'lib.tableHelpers'
local mapGeneration = require 'lib.mapGeneration'
local netSync = require "lib.netSync"
local netMessaging = require 'lib.netMessaging'
local bitser = require 'lib.bitser'
local spriteSheets = require 'lib.spriteSheets'
local enet = require 'enet'

local client = {}

local seed = os.time()
print("Random Seed:", seed)
math.randomseed(seed)

local host
local server
local clientName

function client:enter(previous, mode, serverIp, playerName) -- runs every time the state is entered
  
  assets.snd_music:play()
  assets.snd_music:setLooping(true)
  
  local title = 'the-john-game: mode = ' .. mode .. ', serverIp = ' .. serverIp
  love.window.setTitle( title )
  print(title)

  if mode == 'server' then
    local serverThread = love.thread.newThread("states/serverThread.lua")
    serverThread:start()
  end

  host = enet.host_create()
  server = host:connect(serverIp .. ":6789")
  print("Client started listening on " .. host:get_socket_address())
  
  clientName = playerName
end

local keyboardHandingSystem = require("systems.client.KeyboardHandlingSystem")()
local buttonsPressed = keyboardHandingSystem.buttonsPressed
local state = 'disconnected'

function client:draw()
  local dt = love.timer.getDelta()
  
  -- Apply state messages from server
  local event = host:service()
  while event do
    if event.type == "receive" then
      local message = bitser.loads(event.data)
      if state == 'waiting' then
        if message.seed ~= nil then
          local initialEntities = {}
          for _,addMes in ipairs(message.a) do
            table.insert(initialEntities, netMessaging.messageToEntity(addMes))
          end
          self.initialiseClient(message.seed, message.w, message.h, message.cursorId, initialEntities)
          buttonsPressed.clientName = clientName
          state = 'initialised'
          print('initialised')
        end
      else
        netMessaging.applyMessage(client_world, message)
      end

    elseif event.type == "connect" then
      print(event.peer, "connected.")
      state = 'waiting'
      print('waiting')

    elseif event.type == "disconnect" then
      print(event.peer, "disconnected. Lost connection to server.")
      gamestate.switch(MenuState)
    end
    event = host:service()
  end

  -- Send keys pressed to server
  if state == 'initialised' and  th.countElements(buttonsPressed) > 0 then
    local message = buttonsPressed
    local compressed = bitser.dumps(message)
    print(th.dump(message))
    print("pushing " .. #compressed .. " bytes")
    server:send(compressed)
  end

  if client_world ~= nil then
    -- Client tick
    client_world:update(dt)
  end

  love.graphics.print("Current FPS: " .. tostring(love.timer.getFPS()), 10, 10)
end

function client:keypressed(key)
  if not keyboardHandingSystem:mapKeypress(key) then return end
  keyboardHandingSystem.keysChanged[key] = true
  assets.snd_cursorBlip:play()
end

function client:keyreleased(key)
  keyboardHandingSystem.keysChanged[key] = nil
end

function client:threaderror(thread, errorstr)
  print("Thread error!\n"..errorstr)
end


function client.initialiseClient(seed, w, h, cursorId, initialEntities)
  print("Random Seed:", seed)
  math.randomseed(seed)

  local cursor = th.first(initialEntities, function(e) return e.netId == cursorId end)

  local tileMap = mapGeneration.generateTileMap(w, h)
  local logisticsMap = mapGeneration.generateLogisticsMap(w, h)
  local oreMap = mapGeneration.generateOreMap(w, h)

  -- Global camera
  camera = gamera.new(0, 0, w * 16, h * 16)
  camera:setScale(4)
  camera:setPosition(cursor.x * 16 + 8, cursor.y * 16 + 8)

  local animationState = { animationOffset = 0 }
  local tileBounds = { xStart = 0, xEnd = 0, yStart = 0, yEnd = 0 }

  -- Client ECS System
  client_world = tiny.world(
    require("systems.client.CameraTrackingSystem")(camera, cursor),
    require("systems.client.VisibleTilesSystem")(tileBounds),
    require("systems.client.MoveAnimationSystem")(),
    require("systems.client.FullscreenSwitchingSystem")(buttonsPressed),
    require("systems.client.TileMapRenderSystem")(tileMap, oreMap, w, h, tileBounds),
    require("systems.client.RailDrawingSystem")(tileBounds),
    require("systems.client.RailGhostChainDrawingSystem")(),
    require("systems.client.SelectedDrawingSystem")(),
    require("systems.client.PathDrawingSystem")(),
    require("systems.client.SpriteSystem")(animationState, tileBounds),
    require("systems.client.CarryDrawingSystem")(tileBounds),
    require("systems.client.ResourceCountSystem")(),
    require("systems.client.TopSpriteSystem")(animationState, tileBounds), 
    require("systems.client.MiningVisualisationSystem")(tileBounds),
    require("systems.client.RocketExhaustVisualisationSystem")(tileBounds),
    require("systems.client.BuildingPlacementDrawingSystem")(cursor),
    require("systems.client.CursorNameSystem")(cursor, tileBounds),
    require("systems.client.OverviewMapSystem")(cursor, buttonsPressed, w, h, oreMap),
    require("systems.client.UtilityPodSoundSystem")(),
    require("systems.client.UnitHudSystem")(),
    require("systems.client.StatsHudSystem")(),
    require("systems.client.AuctionHudSystem")(buttonsPressed),
    keyboardHandingSystem,
    unpack(initialEntities)
  )
end

return client
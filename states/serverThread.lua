-- Required for breakpoints in the thead (will need to turn of the other require("mobdebug").start() in main)
--require("mobdebug").start()

tiny = require "lib.tiny"
class = require "lib.30log"
local love_timer = require 'love.timer'
local server = require 'states.server'

print("starting the server")

-- Fixed timestep
local dt = 0.1 -- 10Hz
local currentTime = love_timer.getTime()
local accumulator = 0.0

while true do
  local newTime = love_timer.getTime()
  local frameTime = newTime - currentTime
  currentTime = newTime
  accumulator = accumulator + frameTime
  
  while accumulator >= dt do
    
    -- Perform a tick
    server.tick(dt)

    accumulator = accumulator - dt
  end

  love_timer.sleep( dt - accumulator + currentTime - love_timer.getTime() )
end
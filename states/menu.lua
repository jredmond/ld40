local gamestate = require 'lib.gamestate'
local suit = require 'lib.suit'
local nameGeneration = require "lib.nameGeneration"
local assets = require 'lib.assets'
local menu = {}

-- States
MenuState = menu
ClientState = require 'states.client'


local playerName = {text = nameGeneration.newPlayerName()}
local serverIp = {text = "127.0.0.1"}

function menu:enter()
  love.graphics.setFont(assets.fnt_hud)
  suit.setHit(nil)
end

function menu:update(dt)
    local b_w, b_h = 300, 30
    
    -- buttons stack down from (center,100)
    suit.layout:reset((love.graphics.getWidth() - b_w) / 2,100)
    suit.layout:padding(10,10)
    
    if suit.Button("Start game", suit.layout:row(b_w, b_h)).hit then
      gamestate.switch(ClientState, 'server', 'localhost', playerName.text)
    end
    if suit.Button("Join game", suit.layout:row()).hit then
      gamestate.switch(ClientState, 'client', serverIp.text, playerName.text)
    end
    suit.Input(playerName, suit.layout:row())
    suit.Input(serverIp, suit.layout:row())
end

function menu:draw()
    suit.draw()
end

-- So the input field works
function menu:keypressed(key)
  suit.keypressed(key)
end

function menu:textinput(t)
  suit.textinput(t)
end

return menu
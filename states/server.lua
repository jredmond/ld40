local mapGeneration = require 'lib.mapGeneration'
local Rover = require 'entities.Rover'
local Auction = require 'entities.Auction'
local Cursor = require 'entities.Cursor'
local netSync = require 'lib.netSync'
local netMessaging = require 'lib.netMessaging'
local th = require 'lib.tableHelpers'
local bitser = require 'lib.bitser'
local enet = require 'enet'

-- To use: see server.tick(dt)
local server = {}

local clients = {}

local seed = os.time()
print("Random Seed:", seed)
math.randomseed(seed)

local w, h = 100, 100
local tileMap = mapGeneration.generateTileMap(w, h)
local logisticsMap = mapGeneration.generateLogisticsMap(w, h)
local oreMap = mapGeneration.generateOreMap(w, h)

-- Global ECS instance
local server_world = tiny.world(  
  require("systems.server.PathFollowingSystem")(), 
  require("systems.server.WorkerAISystem")(logisticsMap, oreMap, w, h),
  require("systems.server.PlayerStatsTrackingSystem")(clients),
  require("systems.server.AuctionManagerSystem")(clients),
  -- add initial entities
  netSync.tracked(Rover(2, 3, 1)), 
  netSync.tracked(Rover(4, 2, 2)), 
  netSync.tracked(Rover(6, 3, 3)),
  netSync.tracked(Auction())
)
server_world:refresh()

local host = enet.host_create('*:6789')
print("Server started listening on " .. host:get_socket_address())

local serverTiming = {
  time = 0.0,
  tick = 0
}
netMessaging.serverTiming = serverTiming

-- Server tick function
function server.tick(dt)

  -- clear previous input
  for _,client in pairs(clients) do
    for k in pairs(client.bp) do client.bp[k] = nil end
  end
  
  -- Recieve data from clients
  local event = host:service()
  while event do
    if event.type == "receive" then
      local message = bitser.loads(event.data)
      local peer = clients[event.peer:connect_id()]
      for k,v in pairs(message) do 
        if k == "clientName" then 
          peer.cursor.name = v
        else
          peer.bp[k] = true
        end
      end

    elseif event.type == "connect" then
      print(event.peer, "connected.")
      local clientCount = th.countElements(clients)
      -- Find an empty spot
      local cursorX, cursorY = math.random(1,10), math.random(1,10)
      while logisticsMap[cursorY][cursorX] ~= nil do
        cursorX, cursorY = math.random(1,10), math.random(1,10)
      end
      local cursor = netSync.tracked(Cursor(cursorX, cursorY, '', 0, clientCount + 1))
      local rover = netSync.tracked(Rover(cursor.x, cursor.y, cursor.teamId))
      local bp = {}
      
      local peerEntities = {
        cursor,
        require("systems.server.CursorActionSystem")(cursor, bp, w, h, logisticsMap, serverTiming),
        require("systems.server.PathFindingSystem")(cursor, w, h),
        require("systems.server.RailLayingSystem")(cursor, bp, logisticsMap),
        require("systems.server.BuildingPlacementSystem")(cursor, bp, logisticsMap)
      }
      server_world:add(rover, unpack(peerEntities))
      server_world:refresh()
      
      -- send the new cursor to other clients too (as refresh prevents the sending of cursor)
      for id,client in pairs(clients) do
        if id ~= event.peer:connect_id() then
          local message = netMessaging.buildMessage({}, { cursor, rover }, {})
          local compressed = bitser.dumps(message)
          print(th.dump(message))
          print("pushing " .. #compressed .. " bytes")
          client.peer:send(compressed)
        end
      end
      
      local message = netMessaging.buildMessage({}, server_world.entities, {})
      message.seed = seed
      message.w = w
      message.h = h
      message.cursorId = cursor.netId
      
      local compressed = bitser.dumps(message)
      print(th.dump(message))
      print("pushing " .. #compressed .. " bytes")
      event.peer:send(compressed)
      
      clients[event.peer:connect_id()] = { bp = bp, cursor = cursor, state = 'initialSent', peer = event.peer, peerEntities = peerEntities }

    elseif event.type == "disconnect" then
      print(event.peer, "disconnected.")
      local disconnectedId = nil
      for k,client in pairs(clients) do
        if client.peer == event.peer then
          disconnectedId = k
          break;
        end
      end
      server_world:remove(unpack(clients[disconnectedId].peerEntities))
      clients[disconnectedId] = nil
      for id,client in pairs(clients) do
        if id ~= event.peer:connect_id() then
          local message = netMessaging.buildMessage({}, {}, server_world.entitiesToRemove)
          local compressed = bitser.dumps(message)
          print(th.dump(message))
          print("pushing " .. #compressed .. " bytes")
          client.peer:send(compressed)
        end
      end
      server_world:refresh()

    end
    event = host:service()
  end

  -- tick
  server_world:update(dt)

  -- Add tracking to new entities
  for i,v in ipairs(server_world.entitiesToChange) do
    local tracked = netSync.tracked(v)
    server_world.entitiesToChange[i] = tracked
  end
  -- Add new entities to the logistic map
  for i,v in ipairs(server_world.entitiesToChange) do
    if v.isLog then
      logisticsMap[v.y][v.x] = v
    end
  end

  -- Prepare a state update message (to be sent to clients)
  local stateMessage = netMessaging.buildMessage(netSync.changes, server_world.entitiesToChange, server_world.entitiesToRemove)
  if stateMessage ~= nil then
    local compressed = bitser.dumps(stateMessage)
    print(th.dump(stateMessage))
    print("pushing " .. #compressed .. " bytes")
    host:broadcast(compressed)
  end
  
  -- clear
  netSync.changes = {}
  
  serverTiming.time = serverTiming.time + dt
  serverTiming.tick = serverTiming.tick + 1
end

return server

local netSync = require "lib.netSync"

local Hq = class "Hq"
local synced = netSync:createSet('resources')

function Hq:init(x, y, teamId, resources, netId, alreadyBuilt)

  self.x = x
  self.y = y
  self.teamId = teamId
  self.netId = netId or netSync:nextId()
  self.synced = synced
  self.spriteIndex = { {x = 0, y = 8}, {x = 1, y = 8}, {x = 2, y = 8}, {x = 3, y = 8}, {x = 4, y = 8}, {x = 5, y = 8}, {x = 6, y = 8}, {x = 7, y = 8} }
  self.spriteIndexTop = { {x = 0, y = 7}, {x = 1, y = 7}, {x = 2, y = 7}, {x = 3, y = 7}, {x = 4, y = 7}, {x = 5, y = 7}, {x = 6, y = 7}, {x = 7, y = 7} }
  self.railConnections = { 1, 1, 1, 1 }
  self.resources = resources or 9
  self.isBuilding = true
  -- Animate it's creation
  self.spriteIndexBuild = { {x = 0, y = 9}, {x = 1, y = 9}, {x = 2, y = 9}, {x = 3, y = 9}, {x = 4, y = 9}, {x = 5, y = 9}, {x = 6, y = 9}, {x = 7, y = 9} }
  self.spriteIndexBuildTop = { {x = 0, y = 10}, {x = 0, y = 10}, {x = 0, y = 10}, {x = 0, y = 10}, {x = 1, y = 10}, {x = 2, y = 10}, {x = 3, y = 10}, {x = 4, y = 10} }
  if alreadyBuilt then
    self.buildStart = nil
  else
    self.buildStart = 0
  end
  self.isLog = true

end
   
return Hq
local netSync = require "lib.netSync"

local Worker = class "Worker"
local synced = netSync:createSet('x', 'y', 'carry', 'isMining', 'move', 'goal')

function Worker:init(x, y, homeX, homeY, name, skills, teamId, netId)

  self.x = x
  self.y = y
  self.name = name
  self.teamId = teamId
  self.netId = netId or netSync:nextId()
  self.synced = synced
  self.spriteIndex = {{x = 4, y = 6}}
  self.spriteIndexMove = { ['r'] = {x = 4, y = 6}, ['l'] = {x = 4, y = 6}, ['d'] = {x = 5, y = 6}, ['u'] = {x = 5, y = 6} }
  self.spriteIndexCarry = { ['r'] = {x = 6, y = 6}, ['l'] = {x = 6, y = 6}, ['d'] = {x = 7, y = 6}, ['u'] = {x = 7, y = 6} }
  self.movementRange = 1
  self.path = {}
  self.move = '' -- '',u,d,l,r
  self.prevMove = ''
  self.prevX = x
  self.prevY = y
  self.offset = 0.0
  self.targetOffset = 0.0
  self.pathTravelled = 0.0
  self.goal = "storage"
  self.timeAtTask = 0.0
  self.carry = 0
  self.skills = skills
  self.carryLoaded = false
  self.home = { x = homeX, y = homeY }
  self.sleepCooldown = 0.0

end

function Worker:setGoal(goalName, path)
  if path ~= nil and #path > 0 then
    self.goal = goalName
    self.path = path
    self.target = path[#path]
  else
    self.goal = "storage"
  end
end

return Worker
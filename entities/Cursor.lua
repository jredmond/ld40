local netSync = require "lib.netSync"

local Cursor = class "Cursor"
local synced = netSync:createSet('x', 'y', 'mode', 'currentBuilding', 'ghostChain', 'tp', 'selectedId', 'name', 'wealth')

function Cursor:init(x, y, name, wealth, teamId, netId)

  self.x = x
  self.y = y
  self.netId = netId or netSync:nextId()
  self.synced = synced
  self.teamId = teamId
  self.wealth = wealth
  self.name = name
  self.spriteIndex = {{x = 0, y = 0}, {x = 1, y = 3}}
  self.isCursor = true
  self.mode = 'none'
  self.selectedUnit = nil
  self.selectedId = 0
  self.currentBuilding = 1
  self.targetPath = {}
  self.tp = '' -- send this over network instead of targetPath
  self.ghostChain = {}  
  self.targetBuildingSprites = {
    {{ x = 4,  y = 5 }}, -- stockpile
    {{ x = 0,  y = 1 }, { x = 0,  y = 2 }, { x = 1,  y = 2 }}, -- mine
    {{ x = 2,  y = 2 }}, -- solar
    {{ x = 3,  y = 2 }}, -- rocket
    {{ x = 2,  y = 3 }}, -- pipe --
    {{ x = 3,  y = 3 }}, -- pipe -|-
    {{ x = 4,  y = 3 }}, -- pipe |
    {{ x = 5,  y = 2 }}, -- ore
    {{ x = 6,  y = 2 }}, -- ice
    {{ x = 3,  y = 5 }}, -- offloader
  }

end

function Cursor:setSelected(unit)
  if unit ~= nil then
    self.selectedUnit = unit
    self.selectedId = unit.netId
  else
    self.selectedUnit = nil
    self.selectedId = 0
  end
end

return Cursor
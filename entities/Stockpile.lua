local netSync = require "lib.netSync"

local Stockpile = class "Stockpile"
local synced = netSync:createSet('resources')

function Stockpile:init(x, y, resources, netId)

  self.x = x
  self.y = y
  self.netId = netId or netSync:nextId()
  self.synced = synced
  self.spriteIndex = {{ x = 4,  y = 5 }}
  self.resources = resources or 3
  self.isLog = true

end
   
return Stockpile
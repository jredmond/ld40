local netSync = require "lib.netSync"

local Auction = class "Auction"
local synced = netSync:createSet('item', 'topBid', 'topBidder', 'winner', 'tr')

function Auction:init(netId)

  self.item = "Bob"
  self.topBid = 23
  self.topBidder = "Jeff"
  self.timeRemaining = 10
  self.tr = 10
  self.winner = ""

  self.netId = netId or netSync:nextId()
  self.synced = synced

end

return Auction
local netSync = require "lib.netSync"

local Solar = class "Solar"
local synced = netSync:createSet('remaining')

function Solar:init(x, y, teamId, remaining, netId)

  self.x = x
  self.y = y
  self.teamId = teamId
  self.synced = synced
  self.netId = netId or netSync:nextId()
  self.spriteIndex = {{ x = 2,  y = 2 }}
  self.railConnections = { 1, 1, 1, 1 }
  self.isBuilding = true
  self.isLog = true
  self.cost = 10
  self.remaining = remaining or self.cost
  if self.remaining > 0 then
    self.inProgress = true
  end

end
   
return Solar
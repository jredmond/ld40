local netSync = require "lib.netSync"

local Rocket = class "Rocket"
local synced = netSync:createSet('x','y', 'move')

function Rocket:init(x, y, teamId, netId)

  self.x = x
  self.y = y
  self.teamId = teamId
  self.synced = synced
  self.netId = netId or netSync:nextId()
  self.spriteIndex = {{ x = 4,  y = 2 }}
  self.isRocket = true
  self.skills = { speed = 6 }
  self.path = {}
  for i=1,10 do
    self.path[i] = { x = x, y = y - (i - 1) }
  end
  self.move = '' -- '',u,d,l,r
  self.prevMove = ''
  self.prevX = x
  self.prevY = y
  self.offset = 0.0
  self.targetOffset = 0.0
  self.pathTravelled = 0.0
  
end
   
return Rocket
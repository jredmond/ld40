local netSync = require "lib.netSync"

local Rail = class "Rail"

function Rail:init(x, y, railConnections, netId)

  self.x = x
  self.y = y
  self.railConnections = railConnections
  self.netId = netId or netSync:nextId()
  if railConnections[1] < 0 or railConnections[2] < 0 or railConnections[3] < 0 or railConnections[4] < 0 then
    self.inProgress = true
  end
  self.isLog = true
end

return Rail
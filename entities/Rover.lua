local netSync = require "lib.netSync"

local Rover = class "Rover"
local synced = netSync:createSet('x', 'y', 'move')

function Rover:init(x, y, teamId, netId)

  self.x = x
  self.y = y
  self.netId = netId or netSync:nextId()
  self.synced = synced
  self.teamId = teamId
  self.spriteIndex = {{x = 4, y = 0}, {x = 6, y = 1}}
  self.spriteIndexMove = { ['r'] = {x = 4, y = 0}, ['l'] = {x = 5, y = 0}, ['d'] = {x = 6, y = 0}, ['u'] = {x = 7, y = 0} }
  self.movementRange = 3
  self.skills = { speed = 8 }
  self.path = {}
  self.move = '' -- '',u,d,l,r
  self.prevMove = ''
  self.prevX = x
  self.prevY = y
  self.offset = 0.0
  self.targetOffset = 0.0
  self.pathTravelled = 0.0

end

return Rover
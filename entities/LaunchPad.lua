local netSync = require "lib.netSync"

local LaunchPad = class "LaunchPad"
local synced = netSync:createSet('remaining')

function LaunchPad:init(x, y, teamId, remaining, netId)

  self.x = x
  self.y = y
  self.teamId = teamId
  self.synced = synced
  self.netId = netId or netSync:nextId()
  self.spriteIndex = {{ x = 3,  y = 2 }}
  self.railConnections = { 1, 1, 1, 1 }
  self.isBuilding = true
  self.isLog = true
  self.cost = 20
  self.remaining = remaining or self.cost
  if self.remaining > 0 then
    self.inProgress = true
  end

end
   
return LaunchPad
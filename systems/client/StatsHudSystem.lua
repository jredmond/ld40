local assets = require "lib.assets"
local StatsHudSystem = tiny.processingSystem(class "StatsHudSystem")
StatsHudSystem.filter = tiny.requireAll("wealth")

local wealths
local offset = 0
function StatsHudSystem:preProcess(dt)
  offset = 0
  wealths = {}
end

function StatsHudSystem:process(e, dt)
  table.insert(wealths, e)
end

local function highestToLowest(a,b)
  return a.wealth > b.wealth
end

function StatsHudSystem:postProcess(dt)
  local sizeX = 100
  local sizeY = 10 + 15 * #wealths
  local top = 30
  local left = 10
  local roundedCornerRadius = 5
  love.graphics.setColor(47,79,79)
  love.graphics.rectangle( 'fill', left, top, sizeX, sizeY, roundedCornerRadius, roundedCornerRadius)
  
  table.sort(wealths, highestToLowest)
  for _,cursor in ipairs(wealths) do
    love.graphics.setFont(assets.fnt_hud)
    local index = (cursor.teamId - 1) % #assets.cursorColours + 1
    local col = assets.cursorColours[index]
    love.graphics.setColor(col[1], col[2], col[3], 255) 
    love.graphics.print(cursor.name .. ': ' .. cursor.wealth, left + 5, top + 5 + offset)
    love.graphics.setColor(255, 255, 255) 
    offset = offset + 15
  end
end

return StatsHudSystem
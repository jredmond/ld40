local spriteSheets = require("lib.spriteSheets")

local TileMapRenderSystem = tiny.system(class "TileMapRenderSystem")

function TileMapRenderSystem:init(tileMap, oreMap, w, h, tileBounds)
  self.tileMap = tileMap
  self.oreMap = oreMap
  self.w = w
  self.h = h
  self.tileBounds = tileBounds
end

function TileMapRenderSystem:update(dt)
  local tb = self.tileBounds
  for y = tb.yStart, tb.yEnd do
    for x = tb.xStart, tb.xEnd do
      local tileId = self.tileMap[y][x]
      local oreId = self.oreMap[y][x]
      self:drawQuad(self.tileSpritePositions[tileId], x, y)
      if oreId ~= nil then
        self:drawQuad(self.oreSpritePositions[oreId], x, y)
      end
    end
  end
end

function TileMapRenderSystem:drawQuad(spriteIndex, x, y)
  local spriteSize = 16
  local spriteSheet = spriteSheets.get(1)
  local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  camera:draw(function()
    -- Expensive todo replace with sprite batching
    love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
  end)
end

-- tile id -> index
TileMapRenderSystem.tileSpritePositions = { 
  [1] = { x = 1, y = 0 },
  [2] = { x = 2, y = 0 } 
}

TileMapRenderSystem.oreSpritePositions = {
  [1] = { x = 5, y = 2 }
}

return TileMapRenderSystem
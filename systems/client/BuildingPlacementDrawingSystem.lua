local spriteSheets = require("lib.spriteSheets")

local BuildingPlacementDrawingSystem = tiny.system(class "BuildingPlacementDrawingSystem")

function BuildingPlacementDrawingSystem:init(cursor)
  self.cursor = cursor
end

function BuildingPlacementDrawingSystem:update(dt)
  
  if self.cursor.mode == 'build' then
      -- building ghost visualisation
    local spriteSize = 16
    local spriteSheet = spriteSheets.get(1)
    -- Expensive todo move out of update loop
    local spriteQuad = love.graphics.newQuad(
      self.cursor.targetBuildingSprites[self.cursor.currentBuilding][1].x * spriteSize, 
      self.cursor.targetBuildingSprites[self.cursor.currentBuilding][1].y * spriteSize, 
      spriteSize, spriteSize, spriteSheet:getDimensions())
    camera:draw(function()
        love.graphics.draw(spriteSheet, spriteQuad, self.cursor.x * spriteSize, self.cursor.y * spriteSize)
      end)
  end
  
end

return BuildingPlacementDrawingSystem

local spriteSheets = require "lib.spriteSheets"
local assets = require "lib.assets"

local UnitHudSystem = tiny.processingSystem(class "UnitHudSystem")
UnitHudSystem.filter = tiny.requireAll("goal", "carry")

function UnitHudSystem:init()
  self.psystem = self:initParticleSystem()
end

local hudUnitOffset = 0
local anyIdle = false

function UnitHudSystem:preProcess(dt)
  hudUnitOffset = 0
  
  if anyIdle then
    self.psystem:update(dt)
  end
  anyIdle = false
end

function UnitHudSystem:process(e, dt)
  -- background rect
  local screenBottom = love.graphics.getHeight()
  local margin = 10
  local sizeX = 100
  local sizeY = 120
  local top = screenBottom - sizeY - margin
  local left = margin + hudUnitOffset
  local roundedCornerRadius = 5
  love.graphics.setColor(47,79,79)
  love.graphics.rectangle( 'fill', left, top, sizeX, sizeY, roundedCornerRadius, roundedCornerRadius)
  
  -- unit name
  love.graphics.setFont(assets.fnt_hud)
  love.graphics.setColor(255, 255, 255)
  love.graphics.print(e.name, left + 5, top + 5)
  
  -- draw sprite
  local spriteSize = 16
  local spriteIndex = e.spriteIndex[1]
  local spriteId = e.teamId or 1
  local spriteSheet = spriteSheets.get(spriteId)
  local spriteLeft = (sizeX - spriteSize * 4) / 2
  -- Expensive todo move out of update loop
  local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  love.graphics.draw(spriteSheet, spriteQuad, left + spriteLeft, top + 20, 0, 4, 4)

  -- draw if idle
  if e.goal == 'storage' and e.x == e.home.x and e.y == e.home.y then
    love.graphics.draw(self.psystem, left + sizeX / 2 + 10, top + 25)
    anyIdle = true
  end

  -- draw carrying level
  love.graphics.setFont(assets.fnt_hud)
  love.graphics.setColor(255, 255, 255)
  love.graphics.print("Carry " .. e.skills.carry, left + 5, top + 60)
  love.graphics.print("Speed " .. e.skills.speed, left + 5, top + 80)
  love.graphics.print("Mine  " .. e.skills.mine, left + 5, top + 100)
  --love.graphics.print("G: " .. e.goal, left + 5, top + 80)

  hudUnitOffset = hudUnitOffset + sizeX + margin
end


-- Returns a white Z
local function initZImage()
  local imageData = love.image.newImageData(5, 5)
  imageData:setPixel(0, 0, 255, 255, 255, 255)
  imageData:setPixel(1, 0, 255, 255, 255, 255)
  imageData:setPixel(2, 0, 255, 255, 255, 255)
  imageData:setPixel(3, 0, 255, 255, 255, 255)
  imageData:setPixel(4, 0, 255, 255, 255, 255)
  
  imageData:setPixel(3, 1, 255, 255, 255, 255)
  imageData:setPixel(2, 2, 255, 255, 255, 255)
  imageData:setPixel(1, 3, 255, 255, 255, 255)
  
  imageData:setPixel(0, 4, 255, 255, 255, 255)
  imageData:setPixel(1, 4, 255, 255, 255, 255)
  imageData:setPixel(2, 4, 255, 255, 255, 255)
  imageData:setPixel(3, 4, 255, 255, 255, 255)
  imageData:setPixel(4, 4, 255, 255, 255, 255)

  return love.graphics.newImage( imageData )
end

-- Initialise a spark fountain particle effect
function UnitHudSystem:initParticleSystem()
  local psystem = love.graphics.newParticleSystem(initZImage(), 32)
	psystem:setParticleLifetime(1.5, 1.5)
	psystem:setEmissionRate(1)
  psystem:setSpeed(20)
  psystem:setTangentialAcceleration(-12,12)
  psystem:setDirection(-math.pi/4)
	psystem:setColors(255, 255, 255, 255, 255, 255,	255, 0) -- Fade to transparency.
  return psystem
end

return UnitHudSystem
local MoveAnimationSystem = tiny.processingSystem(class "MoveAnimationSystem")
MoveAnimationSystem.filter = tiny.requireAll('x', 'y', 'move', 'prevMove', 'offset', 'targetOffset')

local TILES_PER_SEC = 0.5

function MoveAnimationSystem:process(e, dt)
  local speedMultiplier = e.skills.speed ~= nil and e.skills.speed or 1
  
  if e.move ~= '' then
    e.targetOffset = e.targetOffset + TILES_PER_SEC * dt * speedMultiplier
  end
  
  if e.prevX ~= e.x or e.prevY ~= e.y or e.prevMove ~= e.move then
    e.targetOffset = 0.0

    if e.prevX ~= e.x or e.prevY ~= e.y then
      if e.prevMove == e.move then
        e.offset = e.offset - 1
      else
        e.offset = 0.0
      end
    end
    
    e.prevMove = e.move
    e.prevX = e.x
    e.prevY = e.y
  end
  
  -- client interpolation/rubber-banding
  local diff = e.targetOffset - e.offset
  if diff < 0.01 then
    e.offset = e.targetOffset
  else
    e.offset = e.offset + diff * dt * TILES_PER_SEC * 2 * speedMultiplier
  end
end

return MoveAnimationSystem
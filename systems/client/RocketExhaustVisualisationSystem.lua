local RocketExhaustVisualisationSystem = tiny.processingSystem(class "RocketExhaustVisualisationSystem")

RocketExhaustVisualisationSystem.filter = tiny.requireAll("x", "y", "path", "isRocket")

function RocketExhaustVisualisationSystem:init(tileBounds)
  self.tileBounds = tileBounds
  self.psystem = self:initParticleSystem()
end

local anyRocketsInFrame = false

function RocketExhaustVisualisationSystem:process(e, dt)
  local x, y = e.x, e.y
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end

  if e.path then
    anyRocketsInFrame = true
    camera:draw(function()
      love.graphics.draw(self.psystem, x * 16 + 8, y * 16 + 16)
    end)
  end
end

function RocketExhaustVisualisationSystem:postProcess(dt)
  if anyRocketsInFrame then
    self.psystem:update(dt)
  end
  anyRocketsInFrame = false
end

-- Returns a white pixel
local function initPixelImage()
  local imageData = love.image.newImageData(1, 1)
  imageData:setPixel(0, 0, 255, 255, 255, 255)
  return love.graphics.newImage( imageData )
end

-- Initialise a spark fountain particle effect
function RocketExhaustVisualisationSystem:initParticleSystem()
  local psystem = love.graphics.newParticleSystem(initPixelImage(), 32)
	psystem:setParticleLifetime(0.1, 1) -- Particles live at least 2s and at most 5s.
	psystem:setEmissionRate(20)
  psystem:setSpeed(40)
  psystem:setDirection(math.pi/2)
  psystem:setSpread(math.pi/5)
	psystem:setLinearAcceleration(0, 100)
	psystem:setColors(255, 255, 255, 255, 255, 165,	0, 100) -- Fade to transparency.
  return psystem
end

return RocketExhaustVisualisationSystem
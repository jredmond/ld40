local assets = require 'lib.assets'

local CursorNameSystem = tiny.processingSystem(class "CursorNameSystem")

CursorNameSystem.filter = tiny.requireAll("x", "y", "isCursor")

function CursorNameSystem:init(myCursor, tileBounds)
  self.myCursor = myCursor
  self.tileBounds = tileBounds
end

function CursorNameSystem:process(cursor, dt)
  local x, y = cursor.x, cursor.y
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end

  if cursor ~= self.myCursor then
    local spriteSize = 16
    camera:draw(function()
      love.graphics.setFont(assets.fnt_small)
      love.graphics.print(cursor.name, (x + 1) * spriteSize, y * spriteSize, 0, 0.5, 0.5)
    end)
  end
end

return CursorNameSystem
local CameraTrackingSystem = tiny.system(class "CameraTrackingSystem")

function CameraTrackingSystem:init(camera, cursor)
  self.cursor = cursor
  self.camera = camera
end

function CameraTrackingSystem:update(dt)
  local l, t, w, h = self.camera:getVisible()
  local x, y = self.cursor.x * 16 + 8, self.cursor.y * 16 + 8
 
  -- if the cursor moves within 2 tiles of the edge of the screen translate the camera to maintain the 2 tile buffer
  local tilesToBuffer = 2
  local buff = tilesToBuffer * 16 + 8
  local lBound, rBound, uBound, dBound = l + buff, l + w - buff, t + buff, t + h - buff
  local xOffset = x - lBound < 0 and x - lBound or x - rBound > 0 and x - rBound or 0
  local yOffset = y - uBound < 0 and y - uBound or y - dBound > 0 and y - dBound or 0
  self:offsetCamera(xOffset, yOffset, dt)
end

-- Move the camera at a constant velocity
local pixelsPerSec = 100
function CameraTrackingSystem:offsetCamera(xOffset, yOffset, dt)
  local dx = xOffset ~= 0 and (xOffset / math.abs(xOffset)) * math.min(pixelsPerSec * dt, math.abs(xOffset)) or 0
  local dy = yOffset ~= 0 and (yOffset / math.abs(yOffset)) * math.min(pixelsPerSec * dt, math.abs(yOffset)) or 0
  if dx ~= 0 or dy ~= 0 then
    local x, y = self.camera:getPosition()
    self.camera:setPosition(x + dx, y + dy)
  end
end

return CameraTrackingSystem
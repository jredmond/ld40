local VisibleTilesSystem = tiny.system(class "VisibleTilesSystem")

function VisibleTilesSystem:init(tileBounds)
  self.tileBounds = tileBounds
end

function VisibleTilesSystem:update(dt)
  -- updated the tiles that are visible by the camera
  local l,t,w,h = camera:getVisible()
  local spriteSize = 16
  local tb = self.tileBounds
  tb.xStart, tb.xEnd = math.floor(l/spriteSize), math.floor((l+w-1)/spriteSize)
  tb.yStart, tb.yEnd = math.floor(t/spriteSize), math.floor((t+h-1)/spriteSize)
end

return VisibleTilesSystem
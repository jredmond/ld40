local assets = require "lib.assets"

local AuctionHudSystem = tiny.processingSystem(class "AuctionHudSystem")
AuctionHudSystem.filter = tiny.requireAll('topBid')

function AuctionHudSystem:init(buttonsPressed)
  self.buttonsPressed = buttonsPressed
end

local showWindow = false
local screenW, screenH
local margin = 120
local roundedCornerRadius = 10

function AuctionHudSystem:preProcess(dt)
  if self.buttonsPressed.auctions then
      self.showWindow = not self.showWindow
  end
  
  if self.showWindow then
    -- background rect
    screenW = love.graphics.getWidth()
    screenH = love.graphics.getHeight()
    love.graphics.setColor(47,79,79)
    love.graphics.rectangle('fill', margin, margin, screenW - 2 * margin, screenH - 2 * margin, roundedCornerRadius, roundedCornerRadius)
    love.graphics.setColor(255,255,255)
    love.graphics.setFont(assets.fnt_hud)
    love.graphics.print("Resupply Cargo", margin + 5, margin + 5)
  end
end

function AuctionHudSystem:process(e, dt)
  if self.showWindow then

    local centerX = screenW / 2

    love.graphics.setFont(assets.fnt_hud)
    love.graphics.print("Top Bid: " .. e.topBid, margin + 5, margin + 5 + 20)
    love.graphics.print("Top Bidder: " .. e.topBidder, margin + 5, margin + 5 + 35)
    love.graphics.print("Time Remaining: " .. e.tr, margin + 5, margin + 5 + 50)

    love.graphics.print("Item on offer: " .. e.item, centerX + 5, margin + 5 + 20)
    
    if #e.winner > 0 then
      love.graphics.setColor(math.random(0,255), math.random(0,255), math.random(0,255))
      love.graphics.print("Winner: " .. e.winner, margin + 5, margin + 5 + 100)
      love.graphics.setColor(255,255,255)
    end

  end
end

return AuctionHudSystem
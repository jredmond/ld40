local th = require 'lib.tableHelpers'

local KeyboardHandlingSystem = tiny.system(class "KeyboardHandlingSystem")

function KeyboardHandlingSystem:init()
  self.buttonsPressed = {}
  self.keysChanged = {}
end

function KeyboardHandlingSystem:mapKeypress(key)
  local bp = self.buttonsPressed
  if key == 'a' then
    bp.left = true
  elseif key == 'd' then
    bp.right = true
  elseif key == 'w' then
    bp.up = true
  elseif key == 's' then
    bp.down = true
  elseif key == 'space' then
    bp.select = true
  elseif key == 'b' then
    bp.build = true
  elseif key == 't' then
    bp.transform = true
  elseif key == 'm' then
    bp.map = true
  elseif key == 'f' then
    bp.toggleFullscreen = true
  elseif key == 'q' then
    bp.auctions = true
  elseif key == 'e' then
    bp.bid = true
  else
    return false
  end
  return true
end

local keyspressed = {}

local timePassed = 0
local timeStarted = 0
local tickSize = 0.15
local startTime = 0.4

-- Resets
function KeyboardHandlingSystem:update(dt)
  local bp = self.buttonsPressed
  -- clear
  for k in pairs(bp) do bp[k] = nil end
  
  if (th.countElements(self.keysChanged) > 0) then
    if (timeStarted > startTime) then
      timePassed = timePassed + dt
    else
      timeStarted = timeStarted + dt
    end
  else 
    timePassed = 0
    timeStarted = 0
  end
  if (timeStarted > startTime) then
    if (timeStarted ~= 5999) then
      for key,value in pairs(self.keysChanged) do
        self:mapKeypress(key)
      end
      timeStarted = 5999
    end
    if (timePassed > tickSize) then
      timePassed = 0
      for key,value in pairs(self.keysChanged) do
        self:mapKeypress(key)
      end
    end
  end
end

return KeyboardHandlingSystem
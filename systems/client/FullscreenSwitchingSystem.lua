local FullscreenSwitchingSystem = tiny.system(class "FullscreenSwitchingSystem")

function FullscreenSwitchingSystem:init(buttonsPressed)
  self.buttonsPressed = buttonsPressed
  self.isFullscreen = false
end

function FullscreenSwitchingSystem:update(dt)
  if self.buttonsPressed.toggleFullscreen then
    self.isFullscreen = not self.isFullscreen
    love.window.setFullscreen(self.isFullscreen)
    camera:setWindow(0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  end
end

return FullscreenSwitchingSystem
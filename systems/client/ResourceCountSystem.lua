local assets = require "lib.assets"

local ResourceCountSystem = tiny.processingSystem(class "ResourceCountSystem")

ResourceCountSystem.filter = tiny.requireAll("x", "y","resources")

function ResourceCountSystem:process(e, dt)
  local spriteSize = 16
  local x, y = e.x * spriteSize + 10, e.y * spriteSize + 9
  local res = e.resources
  if res > 0 and e.buildStart == nil then
    camera:draw(function()
      love.graphics.setFont(assets.fnt_small)
      love.graphics.setColor(0, 0, 0, 255)
      love.graphics.rectangle('fill', x, y, 6, 7)
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.printf(res, x+1, y, 5, "center")
    end)
  end
end

return ResourceCountSystem
local spriteSheets = require("lib.spriteSheets")

local CarryDrawingSystem = tiny.processingSystem(class "CarryDrawingSystem")
CarryDrawingSystem.filter = tiny.requireAll("x", "y", "carry", "spriteIndexCarry")

function CarryDrawingSystem:init(tileBounds)
  self.tileBounds = tileBounds
end

local lean = {
  [''] =  { x = 0, y = 0 },
  ['u'] = { x = 0, y = 1 },
  ['d'] = { x = 0, y = -1 },
  ['l'] = { x = 1, y = 0 },
  ['r'] = { x = -1, y = 0 },
}

function CarryDrawingSystem:process(e, dt)
  local x, y = e.x, e.y 
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end
  
  if e.carry == 0 or (e.x == e.home.x and e.y == e.home.y) then return end

  local spriteSize = 16
  x, y = self:getRenderCoordinates(e, dt)
  local orient = e.move
  local spriteIndex = self:getSpriteIndex(e, orient)
  local spriteId = e.teamId or 1
  local spriteSheet = spriteSheets.get(spriteId)

  -- Expensive todo move out of update loop
  local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  camera:draw(function()
    local stackPixelOffset = 0
    for i=0,e.carry-1 do
      local lean_x, lean_y = (i * 0.3 * lean[orient].x), (i * 0.3 * lean[orient].y)
      love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize + lean_x, y * spriteSize - stackPixelOffset + lean_y)
      stackPixelOffset = stackPixelOffset + 2
    end
  end)
end

function CarryDrawingSystem:getRenderCoordinates(e)
  if e.move == nil or e.move == '' then
    return e.x, e.y
  end
  -- Interpolate position between cur and next tile for smooth path movement
  local next_x, next_y = self:getNext(e.x, e.y, e.move)
  local x, y = e.x + (next_x - e.x) * e.offset, e.y + (next_y - e.y) * e.offset
  return x, y
end

function CarryDrawingSystem:getNext(x, y, move)
  if move == 'l' then
    return x - 1, y
  elseif move == 'r' then
    return x + 1, y
  elseif move == 'u' then
    return x, y - 1
  elseif move == 'd' then
    return x, y + 1
  end
end

function CarryDrawingSystem:getSpriteIndex(e, orient)

  local hasOrientation = orient ~= nil and orient ~= ''
  
  if hasOrientation then
    return e.spriteIndexCarry[orient]
  else
    return e.spriteIndexCarry['r']
  end
end

return CarryDrawingSystem
local th = require 'lib.tableHelpers'

local SelectedDrawingSystem = tiny.processingSystem(class "SelectedDrawingSystem")
SelectedDrawingSystem.filter = tiny.requireAll('isCursor')

function SelectedDrawingSystem:update(dt)
  
  for _,cursor in ipairs(self.entities) do
    if cursor.mode == 'selected' and cursor.selectedId ~= 0 then
      
      local e = th.first(client_world.entities, function(e) return e.netId == cursor.selectedId end)
      if e then
        -- Red selected transparent underlay
        camera:draw(function()
          love.graphics.setColor(255, 0, 0, 100)
          love.graphics.rectangle("fill", e.x * 16, e.y * 16, 16, 16)
          love.graphics.setColor(255, 255, 255)
        end)
      end
      
    end
  end
  

end
    
return SelectedDrawingSystem
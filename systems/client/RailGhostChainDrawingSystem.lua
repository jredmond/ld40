local railDrawingHelpers = require 'lib.railDrawingHelpers'

local RailGhostChainDrawingSystem = tiny.processingSystem(class "RailGhostChainDrawingSystem")
RailGhostChainDrawingSystem.filter = tiny.requireAll('isCursor')

function RailGhostChainDrawingSystem:process(cursor, dt)

  -- also draw the pending ghost chain
  camera:draw(function()
      for _,e in ipairs(cursor.ghostChain) do
        railDrawingHelpers.draw(e.x, e.y, e.railConnections)
      end
    end)
end
  
return RailGhostChainDrawingSystem
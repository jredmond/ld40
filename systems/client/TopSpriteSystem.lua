local spriteSheets = require("lib.spriteSheets")

local TopSpriteSystem = tiny.processingSystem(class "TopSpriteSystem")
TopSpriteSystem.filter = tiny.requireAll("x", "y", "spriteIndexTop")

function TopSpriteSystem:init(animationState, tileBounds)
  self.animationState = animationState
  self.tileBounds = tileBounds
end

function TopSpriteSystem:process(e, dt)
  -- Top sprite is one y above (for tall buildings etc.)
  local x, y = e.x, e.y - 1
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end
  
  -- Expensive todo move out of update loop
  local spriteId = e.teamId or 1
  local spriteIndex = self:getSpriteIndex(e)
  local spriteSize = 16
  local spriteSheet = spriteSheets.get(spriteId)
  local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  camera:draw(function(l,t,w,h)
    love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
    end)
end

function TopSpriteSystem:getSpriteIndex(e)
  -- Fancy transforming sprite animation progression
  if e.buildStart ~= nil then
    if e.buildStart == 0 then
      e.buildStart = self.animationState.animationOffset
    end
    local i = self.animationState.animationOffset - e.buildStart + 1
    if i < #e.spriteIndexBuildTop then
      return e.spriteIndexBuildTop[i]
    end
    -- finished building animation
    e.buildStart = nil
  end
  
  local animationIndex = math.fmod(self.animationState.animationOffset, #e.spriteIndexTop) + 1
  return e.spriteIndexTop[animationIndex]
end

return TopSpriteSystem
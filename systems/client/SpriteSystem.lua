local spriteSheets = require("lib.spriteSheets")
local assets = require "lib.assets"
local Rover = require 'entities.Rover'

local SpriteSystem = tiny.sortedProcessingSystem(class "SpriteSystem")
SpriteSystem.filter = tiny.requireAll("x", "y", "spriteIndex")

function SpriteSystem:init(animationState, tileBounds)
  self.animationState = animationState
  self.tileBounds = tileBounds
end

function SpriteSystem:compare(e1, e2)
  -- quick way to get building to render under units
  -- if true e1 gets drawn before e2
  if e1.isBuilding == e2.isBuilding then
    return e1.x < e2.x
  end
  
  return e1.isBuilding ~= nil
end

local accumulator = 0.0
local animationInterval = 0.5

function SpriteSystem:preProcess(dt)
  -- Tick the animations at a consistent time interval
  accumulator = accumulator + dt
  while accumulator >= animationInterval do
    self.animationState.animationOffset = self.animationState.animationOffset + 1
    accumulator = accumulator - animationInterval
  end
end

function SpriteSystem:process(e, dt)
  local x, y = e.x, e.y 
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end
  if e.home and e.x == e.home.x and e.y == e.home.y then return end

  local spriteSize = 16
  x, y = self:getRenderCoordinates(e, dt)
  local orient = e.move
  local spriteIndex = self:getSpriteIndex(e, orient)
  local spriteId = e.teamId or 1
  local spriteSheet = spriteSheets.get(spriteId)

  -- Expensive todo move out of update loop
  local spriteQuad = love.graphics.newQuad(spriteIndex.x * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  camera:draw(function()
    if e.isCursor then 
      local index = (e.teamId - 1) % #assets.cursorColours + 1
      local col = assets.cursorColours[index]
      love.graphics.setColor(col[1], col[2], col[3]) 
    elseif e.remaining ~= nil and e.remaining > 0 then
      love.graphics.setColor(255, 255, 255, 60)
    end
    love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
    love.graphics.setColor(255, 255, 255, 255)

    -- fancy sprite build progress bar
    if e.remaining ~= nil and e.remaining > 0 then
      local sx, sy, sw, sh = spriteQuad:getViewport()
      local yNotBuilt = math.ceil(e.remaining / e.cost * spriteSize)
      spriteQuad:setViewport(sx, sy + yNotBuilt, sw, sh - yNotBuilt)
      love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize + yNotBuilt)
    end
  end)
end

function SpriteSystem:getRenderCoordinates(e)
  if e.move == nil or e.move == '' then
    return e.x, e.y
  end
  -- Interpolate position between cur and next tile for smooth path movement
  local next_x, next_y = self:getNext(e.x, e.y, e.move)
  local x, y = e.x + (next_x - e.x) * e.offset, e.y + (next_y - e.y) * e.offset
  return x, y
end

function SpriteSystem:getNext(x, y, move)
  if move == 'l' then
    return x - 1, y
  elseif move == 'r' then
    return x + 1, y
  elseif move == 'u' then
    return x, y - 1
  elseif move == 'd' then
    return x, y + 1
  end
end

function SpriteSystem:getSpriteIndex(e, orient)
  -- Fancy transforming sprite animation progression
  if e.buildStart ~= nil then
    if e.buildStart == 0 then
      assets.snd_transformBlip:play()
      e.buildStart = self.animationState.animationOffset
    end
    local i = self.animationState.animationOffset - e.buildStart + 1
    if i < #e.spriteIndexBuild then
      return e.spriteIndexBuild[i]
    end
  end
  
  local hasOrientation = e.spriteIndexMove ~= nil and orient ~= nil and orient ~= ''
  
  if hasOrientation and e:instanceOf(Rover) then
    assets.snd_driveBlip:play()
  end
  
  -- Special sprite indexes for facing the direction of travel
  if hasOrientation then
    return e.spriteIndexMove[orient]
  else
    -- idle (maybe bobbing if there are multiple indexes)
    local animationIndex = self.animationState.animationOffset % #e.spriteIndex + 1
    return e.spriteIndex[animationIndex]
  end
end

return SpriteSystem
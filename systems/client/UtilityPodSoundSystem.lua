local assets = require "lib.assets"

local UtilityPodSoundSystem = tiny.processingSystem(class "UtilityPodSoundSystem")
UtilityPodSoundSystem.filter = tiny.requireAll("x", "y", "goal", "carry")

local isAnyPodMoving = false

function UtilityPodSoundSystem:process(e, dt)
  if not (e.goal == "storage" and e.x == e.home.x and e.y == e.home.y) then
    isAnyPodMoving = true
  end
end

function UtilityPodSoundSystem:postProcess(dt)
  if isAnyPodMoving and not assets.snd_workerMove:isPlaying() then
    assets.snd_workerMove:setLooping(true)
    assets.snd_workerMove:play()
  elseif not isAnyPodMoving and assets.snd_workerMove:isPlaying() then
    assets.snd_workerMove:stop()
  end
  isAnyPodMoving = false
end

return UtilityPodSoundSystem
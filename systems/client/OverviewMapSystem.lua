local OverviewMapSystem = tiny.system(class "OverviewMapSystem")

function OverviewMapSystem:init(cursor, buttonsPressed, w, h, oreMap)
  self.cursor = cursor
  self.buttonsPressed = buttonsPressed
  self.mapOpen = false
  self.filter = tiny.requireAll('x', 'y', tiny.rejectAll('isCursor'))
  self.w = w
  self.h = h
  self.oreMap = oreMap
  
  self.minimapdata = love.image.newImageData(w * 3, h * 3)
  self.minimap = love.graphics.newImage(self.minimapdata)
  
  
  self.mapPosX = love.graphics.getWidth() / 2 - self.minimapdata:getWidth() / 2
  self.mapPosY = love.graphics.getHeight() / 2 - self.minimapdata:getHeight() / 2
end
  
function OverviewMapSystem:update(dt)
  if self.buttonsPressed.map then
    if not self.mapOpen then
      if self.resourceData == nil then
        self.resourceData = love.image.newImageData(self.w * 3, self.h * 3)
        self.resourceData:mapPixel(function(x, y, r, g, b, a) return self:drawResourceData(self.oreMap, x, y, r, g, b, a ) end)
      end
      self.minimapdata:paste(self.resourceData, 0, 0)
      self:drawEntities()
      self:setPixel(self.cursor.x*3 + 1, self.cursor.y*3 + 1)
      self.minimap:refresh()
      self.mapOpen = true
    else
      self.mapOpen = false
    end
  end
  
  if self.mapOpen then
    love.graphics.draw(self.minimap, self.mapPosX, self.mapPosY)
  end
end

-- A rail edge is one pixel in the 3x3 tiles
function OverviewMapSystem:drawEntities()
  
  for _, e in ipairs(self.entities) do
    local x, y = e.x * 3, e.y * 3
    if e.railConnections ~= nil and not e.isBuilding then
      self:setPixel(x + 1, y + 1, math.max(unpack(e.railConnections)))
      if e.railConnections[1] > 0 then
        self:setPixel(x + 1, y, e.railConnections[1])
      end
      if e.railConnections[2] > 0 then
        self:setPixel(x + 1, y + 2, e.railConnections[2])
      end
      if e.railConnections[3] > 0 then
        self:setPixel(x, y + 1, e.railConnections[3])
      end
      if e.railConnections[4] > 0 then
        self:setPixel(x + 2, y + 1, e.railConnections[4])
      end
    else
      -- Draw the other non rail entities
      for i=0,2 do
        for j=0,2 do
          self:setPixel(x + i, y + j, e.teamId)
        end
      end
      
    end
  end
end

function OverviewMapSystem:setPixel(x, y, teamId)
  if teamId == nil then
    self.minimapdata:setPixel(x, y, 255, 255, 255, 255)
  elseif teamId == 1 then
    self.minimapdata:setPixel(x, y, 255, 0, 0, 255)
  elseif teamId == 2 then
      self.minimapdata:setPixel(x, y, 0, 255, 0, 255)
  elseif teamId == 3 then
      self.minimapdata:setPixel(x, y, 100, 100, 255, 255)
  end
end

-- Each tile in minimap is represented by 3x3 pixel
function OverviewMapSystem:drawResourceData(oreMap, x, y, r, g, b, a)
  local xIndex = math.floor(x / 3)
  local yIndex = math.floor(y / 3)
    
  if oreMap[yIndex][xIndex] == 1 then
    return 117,0,255,255
  end
  return 0, 0, 0, 255
end


return OverviewMapSystem
local railDrawingHelpers = require 'lib.railDrawingHelpers'

local RailDrawingSystem = tiny.processingSystem(class "RailDrawingSystem")
RailDrawingSystem.filter = tiny.requireAll("x", "y", "railConnections")

function RailDrawingSystem:init(tileBounds)
  self.tileBounds = tileBounds
end

function RailDrawingSystem:process(e, dt)
  local x, y = e.x, e.y 
  local tb = self.tileBounds
  if x < tb.xStart or x > tb.xEnd or y < tb.yStart or y > tb.yEnd then return end
  
  if not e.isBuilding then
    camera:draw(function() railDrawingHelpers.draw(x, y, e.railConnections) end)
  end
end
  
return RailDrawingSystem
local spriteSheets = require "lib.spriteSheets"
local pathSerialize = require "lib.pathSerialize"

local PathDrawingSystem = tiny.processingSystem(class "PathDrawingSystem")
PathDrawingSystem.filter = tiny.requireAll('isCursor')

function PathDrawingSystem:process(cursor, dt)
  if cursor.mode == 'selected' then
    
    -- fancy arrow
    if cursor.targetPath ~= nil then
      camera:draw(function()
          self:drawTiledArrow(cursor.tp)
        end)
    end
    
  end
end

function PathDrawingSystem:drawTiledArrow(pathString)
  local path = pathSerialize.fromString(pathString)
  if #path > 1 then
    -- draw start
    local code = '*' .. self:getEdge(path[1], path[2])
    self:drawRotSprite(path[1], self.spriteIndexLookup[code])
    -- draw body
    for i=2, #path-1 do
      code = self:getEdge(path[i], path[i-1]) .. self:getEdge(path[i], path[i+1])
      self:drawRotSprite(path[i], self.spriteIndexLookup[code])
    end
    -- draw arrow end
    code = self:getEdge(path[#path], path[#path-1]) .. '>'
    self:drawRotSprite(path[#path], self.spriteIndexLookup[code])
  end
end

function PathDrawingSystem:drawRotSprite(pos, index)
  if index == nil then
    print("woah")
  end
  local spriteSize = 16
  local spriteSheet = spriteSheets.get(1)
  local spriteQuad = love.graphics.newQuad(index.x * spriteSize, index.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  love.graphics.draw(spriteSheet, spriteQuad, pos.x * spriteSize, pos.y * spriteSize)
end

function PathDrawingSystem:getEdge(curr, neigh)
  if curr.x > neigh.x then
    return 'l'
  elseif curr.x < neigh.x then
    return 'r'
  elseif curr.y > neigh.y then
    return 'u'
  elseif curr.y < neigh.y then
    return 'd'
  end
end

PathDrawingSystem.spriteIndexLookup = {
  ['*u'] = { x = 5, y = 3 },
  ['*d'] = { x = 6, y = 3 },
  ['*l'] = { x = 7, y = 3 },
  ['*r'] = { x = 0, y = 4 },
  ['d>'] = { x = 1, y = 4 },
  ['u>'] = { x = 2, y = 4 },
  ['l>'] = { x = 3, y = 4 },
  ['r>'] = { x = 4, y = 4 },
  ['lr'] = { x = 5, y = 4 },
  ['rl'] = { x = 5, y = 4 },
  ['ud'] = { x = 6, y = 4 },
  ['du'] = { x = 6, y = 4 },
  ['lu'] = { x = 7, y = 4 },
  ['ul'] = { x = 7, y = 4 },
  ['ur'] = { x = 0, y = 5 },
  ['ru'] = { x = 0, y = 5 },
  ['rd'] = { x = 1, y = 5 },
  ['dr'] = { x = 1, y = 5 },
  ['dl'] = { x = 2, y = 5 },
  ['ld'] = { x = 2, y = 5 },  
}
    
return PathDrawingSystem
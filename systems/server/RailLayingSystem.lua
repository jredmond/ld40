local netSync = require "lib.netSync"
local Rail = require "entities.Rail"

local RailLayingSystem = tiny.system(class "RailLayingSystem")

function RailLayingSystem:init(cursor, buttonsPressed, logisticsMap)
  self.cursor = cursor
  self.buttonsPressed = buttonsPressed
  self.logisticsMap = logisticsMap
end

function RailLayingSystem:update(dt)
  
  local cursor = self.cursor
  local teamId = self.cursor.teamId or 1
  
  if self.buttonsPressed.select then
    if #cursor.ghostChain > 0 then
      -- commit
      for _,g in ipairs(cursor.ghostChain) do
        
        local log = self.logisticsMap[g.y][g.x]
        if log ~= nil and log.railConnections ~= nil then
          -- Add ghosts to existing entity
          self:mergeRailConnections(log, g.railConnections)
        else
          -- Add new entity
          local newRail = Rail(g.x, g.y, g.railConnections)
          newRail.inProgress = true
          self.world:add(newRail)
        end
      end
      cursor.ghostChain = {}
    elseif self.logisticsMap[cursor.y][cursor.x] ~= nil and self.logisticsMap[cursor.y][cursor.x].railConnections ~= nil then
      -- start a new one
      cursor.ghostChain = {{ x = cursor.x, y = cursor.y, railConnections = { 0, 0, 0, 0 } }}
    end
  end
  
  local head = cursor.ghostChain[#self.cursor.ghostChain]
  if head ~= nil and (head.x ~= cursor.x or head.y ~= cursor.y) then
    -- cursor has moved
    local newSteps = self:getExtraPath(head, cursor)
    for _,newStep in ipairs(newSteps) do
      self:updateChain(cursor.ghostChain, newStep, teamId)
    end
    -- raise a property changed
    netSync.add(cursor.netId, "ghostChain", cursor.ghostChain)
  end  
end

function RailLayingSystem:mergeRailConnections(existing, new)
  local added = false
  if new[1] ~= 0 and existing.railConnections[1] == 0 then existing.railConnections[1] = new[1]; added = true  end
  if new[2] ~= 0 and existing.railConnections[2] == 0 then existing.railConnections[2] = new[2]; added = true  end
  if new[3] ~= 0 and existing.railConnections[3] == 0 then existing.railConnections[3] = new[3]; added = true  end
  if new[4] ~= 0 and existing.railConnections[4] == 0 then existing.railConnections[4] = new[4]; added = true  end
  
  if existing.railConnections[1] < 0 or existing.railConnections[2] < 0 or existing.railConnections[3] < 0 or existing.railConnections[4] < 0 then
    existing.inProgress = true
  end
  
  if added then
    -- raise a property changed
    netSync.add(existing.netId, "railConnections", existing.railConnections)
  end
end

function RailLayingSystem:updateChain(chain, newPos, teamId)
  
    -- Detect if the chain has come back onto itself
    -- If it has then trim the chain back to that point
    local backOnItself = false
    for i=#chain, 1, -1 do
      local curr = chain[i]
      if curr.x == newPos.x and curr.y == newPos.y then
        if i ~= #chain then
          -- Trim the exiting edge at i as well
          self:updateEdge(chain[i], chain[i+1], 0)
        end
        for i=#chain, i+1, -1 do
          table.remove(chain)
        end 
        backOnItself = true
        break
      end
    end
    -- Grow the chain
    if not backOnItself then
      local prev = chain[#chain]
      local new = { x = newPos.x, y = newPos.y, railConnections = { 0, 0, 0, 0 } }
      self:updateEdge(prev, new, -teamId)
      table.insert(chain, new)
    end
end

function RailLayingSystem:updateEdge(prev, new, edgeVal)
  if new.x > prev.x then
    prev.railConnections[4] = edgeVal
    new.railConnections[3] = edgeVal
  elseif new.x < prev.x then
    prev.railConnections[3] = edgeVal
    new.railConnections[4] = edgeVal
  elseif new.y > prev.y then
    prev.railConnections[2] = edgeVal
    new.railConnections[1] = edgeVal
  elseif new.y < prev.y then
    prev.railConnections[1] = edgeVal
    new.railConnections[2] = edgeVal
  end
end

 -- taxicab path
function RailLayingSystem:getExtraPath(head, to)
  local dx = to.x - head.x
  local dy = to.y - head.y
  local xStep = to.x > head.x and 1 or -1
  local yStep = to.y > head.y and 1 or -1
  local path = {}
  for x=xStep, dx, xStep do
    table.insert(path, { x = head.x + x, y = head.y })
  end
  for y=yStep, dy, yStep do
    table.insert(path, { x = to.x, y = head.y + y })
  end
  return path
end

return RailLayingSystem
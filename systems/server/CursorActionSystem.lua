local Worker = require "entities.Worker"
local Hq = require "entities.Hq"
local nameGeneration = require "lib.nameGeneration"

local CursorActionSystem = tiny.processingSystem(class "CursorActionSystem")

CursorActionSystem.filter = tiny.requireAll("movementRange")

function CursorActionSystem:init(cursor, buttonsPressed, w, h, logisticsMap, serverTiming)
  self.cursor = cursor
  self.buttonsPressed = buttonsPressed
  self.w, self.h = w, h
  self.logisticsMap = logisticsMap
  self.serverTiming = serverTiming
end

function CursorActionSystem:preProcess(dt)
  self.currentUnit = nil
end

function CursorActionSystem:process(e, dt)
  -- if the cursor is over a unit
  if self.cursor.x == e.x and self.cursor.y == e.y then
    self.currentUnit = e
  end
end

function CursorActionSystem:postProcess(dt)
  -- move cursor
  local c = self.cursor
  if self.buttonsPressed.left then
    c.x = math.max(c.x - 1, 0)
  elseif self.buttonsPressed.right then
    c.x = math.min(c.x + 1, self.w - 1)
  end

  if self.buttonsPressed.up then
    c.y = math.max(c.y - 1, 0)
  elseif self.buttonsPressed.down then
    c.y = math.min(c.y + 1, self.h - 1)
  end

  -- cursor states
  if c.mode == 'none' then
    if self.buttonsPressed.select and self.currentUnit ~= nil and self.currentUnit.goal == nil then
      -- select
      c:setSelected(self.currentUnit)
      c.mode = 'selected'
    end
  elseif c.mode == 'selected' then
    if self.buttonsPressed.select then
      if self.currentUnit == nil then
        -- move to empty tile
        c.selectedUnit.path = c.targetPath
        c:setSelected(nil)
        c.targetPath = {}
        c.tp = ''
        c.mode = 'none'
      elseif c.selectedUnit == self.currentUnit then
        -- deselect by clicking self
        c:setSelected(nil)
        c.mode = 'none'
      else
        -- select other entity
        c:setSelected(self.currentUnit)
      end
    end
    if self.buttonsPressed.transform then
      if c.selectedUnit == self.currentUnit then
        -- transform into hq
        local teamId = self.currentUnit.teamId
        local hq = Hq(self.currentUnit.x, self.currentUnit.y, teamId)
        hq.buildTick = self.serverTiming.tick
        local skills = { carry = math.random(1,10), speed = math.random(1,10), mine = math.random(1,10) }
        self.world:add(hq, Worker(hq.x, hq.y, hq.x, hq.y, nameGeneration.newPodName(), skills, teamId))
        self.world:remove(self.currentUnit)
        c:setSelected(nil)
        c.mode = 'none'
      end
    end
  end
end

return CursorActionSystem
local netSync = require "lib.netSync"
local th = require "lib.tableHelpers"
local Worker = require "entities.Worker"
local nameGeneration = require "lib.nameGeneration"

local AuctionManagerSystem = tiny.processingSystem(class "AuctionManagerSystem")
AuctionManagerSystem.filter = tiny.requireAll('topBid')

function AuctionManagerSystem:init(clients)
  self.clients = clients
end

function AuctionManagerSystem:process(e, dt)
  e.timeRemaining = e.timeRemaining - dt
  
  for _,client in pairs(self.clients) do
    if client.bp.bid then
      e.topBid = e.topBid + 5
      e.topBidder = client.cursor.name
      e.timeRemaining = math.max(e.timeRemaining, 5)
    end
  end
  
  if e.timeRemaining <= 0 then
    e.winner = e.topBidder

    for _,client in pairs(self.clients) do
      if client.cursor.name == e.topBidder then
        -- find place to put winning
        -- what if no hq
        local hq = th.first(self.world.entities, function(ent) return ent.teamId == client.cursor.teamId and ent.isBuilding end)
        if hq then
          -- deliver winning
          local skills = { carry = math.random(1,10), speed = math.random(1,10), mine = math.random(1,10) }
          self.world:add(Worker(hq.x, hq.y, hq.x, hq.y, e.item, skills, client.cursor.teamId))
        end
      end
    end

    -- reset for now
    e.topBid = 23
    e.timeRemaining = 10
    e.topBidder = "Jeff"
    e.item = nameGeneration.newPodName()
  end
  
  e.tr = math.ceil(e.timeRemaining)
end



return AuctionManagerSystem
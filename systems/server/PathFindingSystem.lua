local netSync = require "lib.netSync"
local pathSerialize = require "lib.pathSerialize"

local PathFindingSystem = tiny.system(class "PathFindingSystem")

PathFindingSystem.filter = tiny.requireAll('x', 'y', tiny.rejectAll('isCursor'))

function PathFindingSystem:init(cursor, w, h)
  self.cursor = cursor
  self.w, self.h = w, h
end

local prevToX, prevToY, prevFrom, tempPath
function PathFindingSystem:update(dt)
  local to = self.cursor
  local from = self.cursor.selectedUnit
  if self.cursor.mode == 'selected' and (prevToX ~= to.x or prevToY ~= to.y or prevFrom ~= from) then
    prevToX = to.x
    prevToY = to.y
    prevFrom = from
    
    if from.x == to.x and from.y == to.y then
      self.cursor.targetPath = { { x = from.x, y = from.y } }
      self.cursor.tp = pathSerialize.toString(self.cursor.targetPath)
      tempPath = {}
      return
    elseif self:safeToVisit(to) then
      local chain = (tempPath and #tempPath > 0) 
        and tempPath 
        or #self.cursor.targetPath > 0 
          and self.cursor.targetPath 
          or { { x = from.x, y = from.y } }
      local head = chain[#chain]
      local newSteps = self:getPath(to, head)
      if newSteps ~= nil then
        table.remove(newSteps, 1)
        for _,newStep in ipairs(newSteps) do
          self:updateChain(chain, newStep)
        end
        self.cursor.targetPath = chain
        self.cursor.tp = pathSerialize.toString(self.cursor.targetPath)
        tempPath = {}
        return
      end
    end
    
    if self.cursor.targetPath then
      tempPath = self.cursor.targetPath
      self.cursor.targetPath = nil
    end
  end

end

function PathFindingSystem:updateChain(chain, newPos)
    -- Detect if the chain has come back onto itself
    local backOnItself = false
    for i=#chain, 1, -1 do
      local curr = chain[i]
      if curr.x == newPos.x and curr.y == newPos.y then
        for i=#chain, i+1, -1 do
          table.remove(chain)
        end 
        backOnItself = true
        break
      end
    end
    -- Grow the chain
    if not backOnItself then
      local new = { x = newPos.x, y = newPos.y }
      table.insert(chain, new)
    end
end

-- breadth first search
function PathFindingSystem:getPath(to, from)
  local frontier = { { x = from.x, y = from.y } }
  local cameFrom = {}
  local start = 'start'
  cameFrom[from.y * self.w + from.x] = start
  local nodesChecked = 0
  
  while #frontier > 0 do
    local current = table.remove(frontier, 1)
    
    if current.x == to.x and current.y == to.y then
      local path = {}
      while current ~= start do
        table.insert(path, 1, current)
        current = cameFrom[current.y * self.w + current.x]
      end
      print("nodes evaluated: " .. nodesChecked)
      return path
    end
    
    if nodesChecked >= 100 then
      print("Too many checks aborting: " .. nodesChecked)
      return nil
    end
    
    local neighbors = self:getNeighbours(current)
    for _,neigh in ipairs(neighbors) do
      if cameFrom[neigh.y * self.w + neigh.x] == nil then
        table.insert(frontier, neigh)
        cameFrom[neigh.y * self.w + neigh.x] = current
        nodesChecked = nodesChecked + 1
      end
    end
  end
end

function PathFindingSystem:getNeighbours(pos)
  local all = {
    { x = pos.x - 1, y = pos.y }, 
    { x = pos.x + 1, y = pos.y }, 
    { x = pos.x, y = pos.y - 1 }, 
    { x = pos.x, y = pos.y + 1 }
  }
  local n = {}
  for _,v in ipairs(all) do
    if self:safeToVisit(v) then
      table.insert(n, v)
    end
  end
  return n
end

function PathFindingSystem:safeToVisit(pos)
  local test = self.entities
  for _, e in ipairs(self.entities) do
    if e.x == pos.x and e.y == pos.y then
      return false
    end
  end
  return pos.x >= 0 and pos.x < self.w and pos.y >= 0 and pos.y < self.h
end

return PathFindingSystem
local pathFinding = require "lib.pathFinding"
local netSync = require "lib.netSync"
local Rocket = require "entities.Rocket"
local LaunchPad = require "entities.LaunchPad"

local WorkerAISystem = tiny.processingSystem(class "WorkerAISystem")

WorkerAISystem.filter = tiny.requireAll("x", "y", "goal", "carry")

function WorkerAISystem:init(logisticsMap, oreMap, w, h)
  self.logisticsMap = logisticsMap
  self.oreMap = oreMap
  self.w = w
  self.h = h
end

function WorkerAISystem:process(e, dt)
  
  -- dispatch to current behaviour update method
  local g = e.goal
  if     g == "storage"   then self:storage(e, dt)
  elseif g == "collect"   then self:collect(e, dt)
  elseif g == "mine"      then self:mine(e, dt)
  elseif g == "build"     then self:build(e, dt)
  elseif g == "stockpile" then self:stockpile(e, dt)
  end

end

-- Chillin at the hq with nothing to do
function WorkerAISystem:storage(e, dt)  
  if e.sleepCooldown >= 0 then e.sleepCooldown = e.sleepCooldown - dt end
  
  if e.sleepCooldown <= 0 then
    -- Todo remove iteration
    local buildTileCount = 0
    for y=0,self.h-1 do
      for _,l in pairs(self.logisticsMap[y]) do
        if l.inProgress then
          buildTileCount = buildTileCount + 1
        end
      end
    end
    
    if e.carry == 0 then
      e.loaded = false
    elseif e.carry == e.skills.carry then
      e.loaded = true
    end
    
    -- Switch to new goal if it comes available
    local pathToGhost
    if buildTileCount > 0 then
      print("Finding path to build")
      pathToGhost= self:findPathToNearestGhost(e)
    end
    
    if not e.loaded and pathToGhost ~= nil then
      print("Finding path to resource")
      local pathToRes = self:findPathToNearestResource(e)
      if pathToRes ~= nil then e:setGoal("collect", pathToRes) return end
    end
    
    if not e.loaded then
      print("Finding path to ore")
      local pathToOre = self:findPathToNearestOre(e)
      if pathToOre ~= nil then e:setGoal("mine", pathToOre) return end
    end
    
    if pathToGhost ~= nil and e.carry > 0 then
      e:setGoal("build", pathToGhost)
      return
    end
    
    if e.carry > 0 then
      print("Finding path to stockpile")
      local pathToStockpile = self:findPathToNearestStockpile(e)
      if pathToStockpile ~= nil then e:setGoal("stockpile", pathToStockpile) return end
    end
    -- no build, resources, ore, go to sleep for 5 to reduce searching frequency
    e.sleepCooldown = 5.0
  end

  local target = e.home
  if e.x ~= target.x or e.y ~= target.y then
    self:updatePath(e, target)
    return
  end
end

function WorkerAISystem:collect(e, dt)
  if e.x ~= e.target.x or e.y ~= e.target.y then 
    if e.path == nil then e.goal = "storage" end
    return 
  end
  
  local resourceEntity = self.logisticsMap[e.target.y][e.target.x]
  if resourceEntity ~= nil and resourceEntity.resources > 0 then
    resourceEntity.resources = resourceEntity.resources - 1
    if resourceEntity.resources <= 0 and resourceEntity.isBuilding ~= true then
      self.logisticsMap[e.target.y][e.target.x] = nil
      self.world:remove(resourceEntity)
    end
    e.carry = e.carry + 1
  end
  
  e.goal = "storage"
end

function WorkerAISystem:stockpile(e, dt)
  if e.x ~= e.target.x or e.y ~= e.target.y then 
    if e.path == nil or e.carry <= 0 then e.goal = "storage" end
    return 
  end
  
  local resourceEntity = self.logisticsMap[e.target.y][e.target.x]
  if resourceEntity ~= nil and resourceEntity.resources ~= nil then
    resourceEntity.resources = resourceEntity.resources + 1
    e.carry = e.carry - 1
  end
  
  e.goal = "storage"
end

local occupiedMineTiles = {}

function WorkerAISystem:mine(e, dt)
  if e.x ~= e.target.x or e.y ~= e.target.y then 
    if e.path == nil then e.goal = "storage" end
    e.timeAtTask = 0.0
    return 
  end
  
  local occupant = occupiedMineTiles[e.x .. ',' .. e.y]
  if occupant ~= nil and occupant ~= e then
    -- tile taken, find another
    e.goal = "storage"
    e.timeAtTask = 0.0
    return 
  end
  
  e.timeAtTask = e.timeAtTask + dt
  local timeToMine = 25 / e.skills.mine
  if e.timeAtTask < timeToMine then
    -- Mining in progress
    e.isMining = true
    occupiedMineTiles[e.x .. ',' .. e.y] = e
    return
  end
  
  -- Finish mining
  e.carry = e.carry + 1
  e.isMining = false
  e.timeAtTask = 0.0
  e.goal = "storage"
  occupiedMineTiles[e.x .. ',' .. e.y] = nil
end

function WorkerAISystem:build(e, dt)
  if e.x ~= e.target.x or e.y ~= e.target.y then 
    if e.path == nil then e.goal = "storage" end
    return 
  end
  
  -- at build site build any ghost rail
  local log = self.logisticsMap[e.target.y][e.target.x]
  if log == nil or log.railConnections == nil or not log.inProgress then 
    e.goal = "storage" 
    return 
  end
  self:buildAtTile(e, log)
  e.goal = "storage"
end

function WorkerAISystem:updatePath(e, target)
  if e.x ~= target.x or e.y ~= target.y then
    if e.path == nil or #e.path == 0 or e.path[#e.path].x ~= target.x or e.path[#e.path].y ~= target.y then
      e.path = self:getPath(e, target)
    end
  end
end

-- Add any additional edges to existing rail tile
function WorkerAISystem:buildAtTile(builder, tile)
  -- Build tracks
  local added = false
  if tile.railConnections[1] < 0 then tile.railConnections[1] = builder.teamId; added = true end
  if tile.railConnections[2] < 0 then tile.railConnections[2] = builder.teamId; added = true end
  if tile.railConnections[3] < 0 then tile.railConnections[3] = builder.teamId; added = true end
  if tile.railConnections[4] < 0 then tile.railConnections[4] = builder.teamId; added = true end
  
  if added then
    netSync.add(tile.netId, "railConnections", tile.railConnections)
  end
  
  -- todo put this in a better spot
  if tile.remaining ~= nil and tile.remaining == 1 then
    local instance = class.isInstance(tile) and tile or tile[netSync.privateKey]
    if instance:instanceOf(LaunchPad) then
      self.world:add(Rocket(tile.x, tile.y, tile.teamId))
    end
 end
  
  if tile.remaining ~= nil and tile.remaining > 0 then tile.remaining = tile.remaining - 1; added = true end
  
  if tile.remaining == nil or tile.remaining == 0 then
    tile.inProgress = nil
  end
  
  if added then
    builder.carry = builder.carry - 1
  end
end

-- breadth first search
function WorkerAISystem:findPathToNearestResource(from)
  local hasResources = 
    function(curr)
      local tile = self.logisticsMap[curr.y][curr.x] 
      return tile ~= nil and tile.resources ~= nil and tile.resources > 0
    end
  
  return pathFinding.findNearest(
    from, 
    hasResources, 
    function(curr) return self:getNeighbours(curr) end,
    self.w)
end

-- breadth first search
function WorkerAISystem:findPathToNearestStockpile(from)
  local hasCapacity = 
    function(curr)
      local tile = self.logisticsMap[curr.y][curr.x] 
      return tile ~= nil and tile.resources ~= nil and tile.teamId == from.teamId
    end
  
  return pathFinding.findNearest(
    from, 
    hasCapacity, 
    function(curr) return self:getNeighbours(curr) end,
    self.w)
end

-- breadth first search
function WorkerAISystem:findPathToNearestOre(from)
  return pathFinding.findNearest(
    from, 
    function(curr) return self.oreMap[curr.y][curr.x] ~= nil and occupiedMineTiles[curr.x .. ',' .. curr.y] == nil end, 
    function(curr) return self:getNeighbours(curr) end,
    self.w)
end

-- breadth first search
function WorkerAISystem:findPathToNearestGhost(from)
  return pathFinding.findNearest(
    from, 
    function(curr) local log = self.logisticsMap[curr.y][curr.x]; return log ~= nil and log.inProgress end,
    function(curr) return self:getNeighbours(curr, true) end,
    self.w)
end

-- breadth first search
function WorkerAISystem:getPath(from, to)
  if to == nil then return nil end
  return pathFinding.getPath(from, to, 
    function(curr) return self:getNeighbours(curr) end, 
    self.w)
end

function WorkerAISystem:getNeighbours(pos, includeGhosts)
  local all = {
    { x = pos.x - 1, y = pos.y }, 
    { x = pos.x + 1, y = pos.y }, 
    { x = pos.x, y = pos.y - 1 }, 
    { x = pos.x, y = pos.y + 1 }
  }
  
  local n = {}
  for _,v in ipairs(all) do
    if self:areConnected(pos, v, includeGhosts) then
      table.insert(n, v)
    end
  end
  return n
end

function WorkerAISystem:areConnected(from, to, includeGhosts)
  -- if within map
  if to.x < 0 or to.x >= self.w or to.y < 0 or to.y >= self.h then
    return false
  end
  
  local fromLog = self.logisticsMap[from.y][from.x]
  local toLog = self.logisticsMap[to.y][to.x]
  local fromRc = fromLog ~= nil and fromLog.railConnections or nil
  local toRc = toLog ~= nil and toLog.railConnections or nil

  -- Hop off/on network to get to stockpiles etc
  if toLog ~= nil and (fromRc == nil or toRc == nil) then
    return true
  end

  -- is on logistics network with connections
  if fromRc == nil or toRc == nil then
    return false
  end

  local isEdge = function(e)
    return includeGhosts and e ~= 0 or e > 0
  end

  if from.x > to.x then
    return isEdge(fromRc[3]) and isEdge(toRc[4])
  elseif from.x < to.x then
    return isEdge(fromRc[4]) and isEdge(toRc[3])
  elseif from.y < to.y then
    return isEdge(fromRc[2]) and isEdge(toRc[1])
  elseif from.y > to.y then
    return isEdge(fromRc[1]) and isEdge(toRc[2])
  end
end


return WorkerAISystem
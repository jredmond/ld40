local netSync = require "lib.netSync"
local th = require "lib.tableHelpers"

local PlayerStatsTrackingSystem = tiny.processingSystem(class "PlayerStatsTrackingSystem")
PlayerStatsTrackingSystem.filter = tiny.requireAll('isBuilding', 'resources', 'teamId')

function PlayerStatsTrackingSystem:init(clients)
  self.clients = clients
end

local wealths
function PlayerStatsTrackingSystem:preProcess(dt)
  wealths = {}
end
  
function PlayerStatsTrackingSystem:process(e, dt)
  for _,client in pairs(self.clients) do
    if e.teamId == client.cursor.teamId then
      wealths[client.cursor] = wealths[client.cursor] or 0
      wealths[client.cursor] = wealths[client.cursor] + e.resources
    end
  end
end

function PlayerStatsTrackingSystem:postProcess(dt)
  for cursor,wealth in pairs(wealths) do
    cursor.wealth = wealth
  end
end

return PlayerStatsTrackingSystem
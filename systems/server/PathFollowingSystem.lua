local PathFollowingSystem = tiny.processingSystem(class "PathFollowingSystem")
PathFollowingSystem.filter = tiny.requireAll('path', 'x', 'y')

local TILES_PER_SEC = 0.5

function PathFollowingSystem:process(e, dt)
  if e.path == nil or #e.path == 0 then return end
  
  local speedMultiplier = e.skills.speed ~= nil and e.skills.speed or 1
  
  -- Update the position along the path
  e.pathTravelled = e.pathTravelled + TILES_PER_SEC * dt * speedMultiplier
  while e.pathTravelled > 1 and #e.path > 1 do
    -- We have reached the next tile in the path
    table.remove(e.path, 1)
    local newPos = e.path[1]
    e.x = newPos.x
    e.y = newPos.y
    e.pathTravelled = e.pathTravelled - 1
  end
  
  if #e.path == 1 then
    -- At dest
    local newPos = e.path[1]
    e.x = newPos.x
    e.y = newPos.y
    e.path = nil
    e.move = ''
    e.pathTravelled = 0.0
  elseif #e.path > 1 then
    e.move = self:getCurrentMove(e.path[1], e.path[2])
  end
end

function PathFollowingSystem:getCurrentMove(curr, next)
  if curr.x > next.x then
    return 'l'
  elseif curr.x < next.x then
    return 'r'
  elseif curr.y > next.y then
    return 'u'
  elseif curr.y < next.y then
    return 'd'
  end
end

return PathFollowingSystem
local Solar = require "entities.Solar"
local LaunchPad = require "entities.LaunchPad"
local Stockpile = require "entities.Stockpile"


local BuildingPlacementSystem = tiny.system(class "BuildingPlacementSystem")
BuildingPlacementSystem.filter = tiny.requireAll('x', 'y', tiny.rejectAll('isCursor'))

function BuildingPlacementSystem:init(cursor, buttonsPressed, logisticsMap)
  self.cursor = cursor
  self.buttonsPressed = buttonsPressed
  self.logisticsMap = logisticsMap
end

function BuildingPlacementSystem:update(dt)
  local cursor = self.cursor
  if cursor.mode == 'none' then
    if self.buttonsPressed.build then
      cursor.mode = 'build'
    end
  elseif cursor.mode == 'build' then
    -- build on empty tile
    if self.buttonsPressed.select and self:isTileFreeToBuild(cursor) then
      local building = {
        x = cursor.x,
        y = cursor.y,
        spriteIndex = cursor.targetBuildingSprites[cursor.currentBuilding]
      }
      if cursor.currentBuilding == 1 then
        building = Stockpile(cursor.x, cursor.y)
      end
      if cursor.currentBuilding == 3 then
        building = Solar(cursor.x, cursor.y, cursor.teamId)
      elseif cursor.currentBuilding == 4 then
        building = LaunchPad(cursor.x, cursor.y, cursor.teamId)
      end
      self.world:add(building)
      cursor.mode = 'none'
    elseif self.buttonsPressed.build then
      cursor.currentBuilding = math.fmod(cursor.currentBuilding, #cursor.targetBuildingSprites) + 1
    end
  end
  
end

function BuildingPlacementSystem:isTileFreeToBuild(pos)
  local test = self.entities
  for _, e in ipairs(self.entities) do
    if e.x == pos.x and e.y == pos.y then
      return false
    end
  end
  return true
end

return BuildingPlacementSystem